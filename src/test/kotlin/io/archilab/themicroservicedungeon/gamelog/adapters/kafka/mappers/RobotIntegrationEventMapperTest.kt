package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import RobotSpawnedIntegrationEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.Inventory
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.Planet
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.RobotProperties
import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks.*
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RestorationType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.UpgradeType
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.*
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.*
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.Instant
import java.util.*

class RobotIntegrationEventMapperTest : TestWithH2AndWithoutKafka() {

    private val eventMockFactory = EventMockFactory()
    private val mapper = RobotIntegrationEventMapper()

    private val robotId = UUID.randomUUID()
    private val alive = true
    private val playerId = UUID.randomUUID()
    private val maxHealth = 100
    private val maxEnergy = 60
    private val energyRegeneration = 8
    private val attackDamage = 5
    private val miningSpeed = 10
    private val health = 75
    private val energy = 45
    private val healthLevel = 5
    private val damageLevel = 5
    private val miningSpeedLevel = 5
    private val miningLevel = 5
    private val energyLevel = 5
    private val energyRegenerationLevel = 5
    private val storageLevel = 5

    private val spawnPlanetMock =
        MockPlanet(UUID.randomUUID(), UUID.randomUUID(), 1, ResourceType.COAL.name.lowercase())
    private val spawnInventoryMock = MockRobotInventory(
        MockInventoryResources(0, 0, 0, 0, 0),
        0, 0, 0, true
    )

    private val robotSpawnedIntegrationEvent = eventMockFactory.createRobotSpawnedIntegrationEvent(
        robotId,
        alive,
        playerId,
        maxHealth,
        maxEnergy,
        energyRegeneration,
        attackDamage,
        miningSpeed,
        health,
        energy,
        healthLevel,
        damageLevel,
        miningSpeedLevel,
        miningLevel,
        energyLevel,
        energyRegenerationLevel,
        spawnPlanetMock,
        spawnInventoryMock
    )

    @Test
    fun test_robotAttackedIntegrationEvent() {
        val attacker = FightingRobotProperties(UUID.randomUUID(), 90, 90, true)
        val defender = FightingRobotProperties(UUID.randomUUID(), 70, 90, true)
        val event = eventMockFactory.createRobotAttackedIntegrationEvent(attacker.id, attacker.health, attacker.energy, attacker.alive, defender.id, defender.health, defender.energy, defender.alive)
        val expected = RobotAttackedIntegrationEvent(attacker, defender, Instant.parse(event.timestamp))
        assertClassTypeAndContents(event, expected, RobotAttackedIntegrationEvent::class.java)
    }

    @Test
    fun test_robotMovedIntegrationEvent() {
        val originPlanetId = UUID.randomUUID()
        val destinationPlanetId = UUID.randomUUID()
        val movementDifficulty = 1
        val event = eventMockFactory.createRobotMovedIntegrationEvent(robotId, energy, originPlanetId, movementDifficulty, destinationPlanetId, movementDifficulty)
        val expected = RobotMovedIntegrationEvent(robotId, energy, MovementPlanetProperties(originPlanetId, movementDifficulty), MovementPlanetProperties(destinationPlanetId, movementDifficulty), Instant.parse(event.timestamp))
        assertClassTypeAndContents(event, expected, RobotMovedIntegrationEvent::class.java)
    }

    @Test
    fun test_robotRegeneratedIntegrationEvent() {
        val event = eventMockFactory.createRobotRegeneratedIntegrationEvent(robotId, energy)
        val expected = RobotRegeneratedIntegrationEvent(robotId, energy, Instant.parse(event.timestamp))
        assertClassTypeAndContents(event, expected, RobotRegeneratedIntegrationEvent::class.java)
    }

    @Test
    fun test_robotResourceMinedIntegrationEvent() {
        val minedResource = ResourceType.COAL
        val minedAmount = 1
        val resourceInventory = ResourceInventory(minedAmount, 0, 0, 0, 0)
        val event = eventMockFactory.createRobotResourceMinedIntegrationEvent(
            robotId,
            minedAmount,
            minedResource.name.lowercase(),
            resourceInventory.coal,
            resourceInventory.iron,
            resourceInventory.gem,
            resourceInventory.gold,
            resourceInventory.platin
        )
        val expected = RobotResourceMinedIntegrationEvent(robotId, minedAmount, minedResource, resourceInventory, Instant.parse(event.timestamp))
        assertClassTypeAndContents(event, expected, RobotResourceMinedIntegrationEvent::class.java)
    }

    @Test
    fun test_robotResourceRemovedIntegrationEvent() {
        val removedResource = ResourceType.COAL
        val removedAmount = 1
        val resourceInventory = ResourceInventory(removedAmount, 0, 0, 0, 0)
        val event = eventMockFactory.createRobotResourceRemovedIntegrationEvent(
            robotId,
            removedAmount,
            removedResource.name.lowercase(),
            resourceInventory.coal,
            resourceInventory.iron,
            resourceInventory.gem,
            resourceInventory.gold,
            resourceInventory.platin
        )
        val expected = RobotResourceRemovedIntegrationEvent(robotId, removedAmount, removedResource, resourceInventory, Instant.parse(event.timestamp))
        assertClassTypeAndContents(event, expected, RobotResourceRemovedIntegrationEvent::class.java)
    }

    @Test
    fun test_robotRestoredAttributesIntegrationEvent() {
        val restorationType = RestorationType.HEALTH
        val availableEnergy = 10
        val availableHealth = 50
        val event = eventMockFactory.createRobotRestoredAttributesIntegrationEvent(
            robotId,
            restorationType.type,
            availableEnergy,
            availableHealth
        )
        val expected = RobotRestoredAttributesIntegrationEvent(robotId, restorationType, availableEnergy, availableHealth)
        assertClassTypeAndContents(event, expected, RobotRestoredAttributesIntegrationEvent::class.java)
    }

    @Test
    fun test_robotSpawnedIntegrationEvent() {
        val expected = RobotSpawnedIntegrationEvent(
            RobotProperties(
                robotId,
                alive,
                playerId,
                maxHealth,
                maxEnergy,
                energyRegeneration,
                attackDamage,
                miningSpeed,
                health,
                energy,
                healthLevel,
                damageLevel,
                miningSpeedLevel,
                miningLevel,
                energyLevel,
                energyRegenerationLevel,
                spawnPlanetMock.let {
                    Planet(it.planetId, it.gameWorldId, it.movementDifficulty, it.resourceType)
                },
                spawnInventoryMock.let {
                    Inventory(
                        ResourceInventory(it.resources.coal, it.resources.iron, it.resources.gem, it.resources.gold, it.resources.platin),
                        it.storageLevel, it.usedStorage, it.maxStorage, it.full)
                }
            )
        )
        assertClassTypeAndContents(robotSpawnedIntegrationEvent, expected, RobotSpawnedIntegrationEvent::class.java)
    }

    @Test
    fun test_robotUpgradedIntegrationEvent() {
        val level = 1
        val upgrade = UpgradeType.STORAGE
        val event = eventMockFactory.createRobotUpgradedIntegrationEvent(
            robotId,
            level,
            upgrade.type,
            alive,
            playerId,
            maxHealth,
            maxEnergy,
            energyRegeneration,
            attackDamage,
            miningSpeed,
            health,
            energy,
            healthLevel,
            damageLevel,
            miningSpeedLevel,
            miningLevel,
            energyLevel,
            energyRegenerationLevel,
            spawnPlanetMock,
            spawnInventoryMock
        )
        val robot = RobotProperties(
            robotId,
            alive,
            playerId,
            maxHealth,
            maxEnergy,
            energyRegeneration,
            attackDamage,
            miningSpeed,
            health,
            energy,
            healthLevel,
            damageLevel,
            miningSpeedLevel,
            miningLevel,
            energyLevel,
            energyRegenerationLevel,
            spawnPlanetMock.let {
                Planet(it.planetId, it.gameWorldId, it.movementDifficulty, it.resourceType)
            },
            spawnInventoryMock.let {
                Inventory(
                    ResourceInventory(it.resources.coal, it.resources.iron, it.resources.gem, it.resources.gold, it.resources.platin),
                    it.storageLevel, it.usedStorage, it.maxStorage, it.full)
            }
        )
        val expected = RobotUpgradedIntegrationEvent(robotId, level, upgrade, robot)
        assertClassTypeAndContents(event, expected, RobotUpgradedIntegrationEvent::class.java)
    }

    @Test
    fun test_robotRevealedIntegrationEvent() {
        val planetId = UUID.randomUUID()
        val playerNotion = playerId.toString().substring(0 until 4)
        val event = eventMockFactory.createRobotsRevealedIntegrationEvent(
            robotId,
            planetId,
            playerNotion,
            health,
            energy,
            healthLevel,
            damageLevel,
            miningSpeedLevel,
            miningLevel,
            energyLevel,
            energyRegenerationLevel,
            storageLevel
        )
        val expected = RobotsRevealedIntegrationEvent(
            listOf(
                RevealedRobotProperties(
                    robotId,
                    planetId,
                    playerNotion,
                    health,
                    energy,
                    RobotLevels(
                        healthLevel,
                        damageLevel,
                        miningSpeedLevel,
                        miningLevel,
                        energyLevel,
                        energyRegenerationLevel,
                        storageLevel
                    )
                )
            )
        )

        assertClassTypeAndContents(event, expected, RobotsRevealedIntegrationEvent::class.java)
    }

    @Test
    fun test_exception() {
        assertThrows<MapperUnknownEventTypeException> {
            mapper.mapConsumerRecordToEvent("foobar", robotSpawnedIntegrationEvent.timestamp, robotSpawnedIntegrationEvent.payload)
        }
    }

    private fun assertClassTypeAndContents(event: EventMock, expected: InternalEvent, eventClass: Class<out InternalEvent>) {
        val result = mapper.mapConsumerRecordToEvent(event.type, event.timestamp, event.payload)
        assertThat(result.javaClass).isEqualTo(eventClass)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
    }

}