package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks.*
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.UpgradeType
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.*
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.legacy.*
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.*

class RobotEventMapperTest : TestWithH2AndWithoutKafka() {

    private val eventMockFactory = EventMockFactory()
    private val mapper = RobotEventMapper()

    private val robotId = UUID.randomUUID()
    private val alive = true
    private val playerId = UUID.randomUUID()
    private val maxHealth = 100
    private val maxEnergy = 60
    private val energyRegeneration = 8
    private val attackDamage = 5
    private val miningSpeed = 10
    private val health = 75
    private val energy = 45
    private val healthLevel = 5
    private val damageLevel = 5
    private val miningSpeedLevel = 5
    private val miningLevel = 5
    private val energyLevel = 5
    private val energyRegenerationLevel = 5
    private val storageLevel = 5
    private val coal = 1
    private val iron = 1
    private val gem = 1
    private val gold = 1
    private val platin = 1
    private val usedStorage = coal + iron + gem + gold + platin
    private val maxStorage = usedStorage * 2
    private val full = false

    private val spawnPlanetMock =
        MockPlanet(UUID.randomUUID(), UUID.randomUUID(), 1, ResourceType.COAL.name.lowercase())
    private val spawnInventoryMock = MockRobotInventory(
        MockInventoryResources(coal, iron, gem, gold, platin),
        storageLevel, usedStorage, maxStorage, full
    )
    private val robotSpawnedEvent = eventMockFactory.createRobotSpawnedEvent(
        robotId,
        alive,
        playerId,
        maxHealth,
        maxEnergy,
        energyRegeneration,
        attackDamage,
        miningSpeed,
        health,
        energy,
        healthLevel,
        damageLevel,
        miningSpeedLevel,
        miningLevel,
        energyLevel,
        energyRegenerationLevel,
        spawnPlanetMock,
        spawnInventoryMock
    )

    @Test
    fun test_robotSpawned() {
        val expected = RobotSpawnedEvent(
            RobotProperties(
                robotId,
                alive,
                playerId,
                maxHealth,
                maxEnergy,
                energyRegeneration,
                attackDamage,
                miningSpeed,
                health,
                energy,
                healthLevel,
                damageLevel,
                miningSpeedLevel,
                miningLevel,
                energyLevel,
                energyRegenerationLevel,
                spawnPlanetMock.let {
                    Planet(it.planetId, it.gameWorldId, it.movementDifficulty, it.resourceType)
                },
                spawnInventoryMock.let {
                    Inventory(
                        ResourceInventory(it.resources.coal, it.resources.iron, it.resources.gem, it.resources.gold, it.resources.platin),
                        it.storageLevel, it.usedStorage, it.maxStorage, it.full)
                }
            )
        )
        assertClassTypeAndContents(robotSpawnedEvent, expected, RobotSpawnedEvent::class.java)
    }

    @Test
    fun test_robotMovedEvent() {
        val originPlanetId = UUID.randomUUID()
        val destinationPlanetId = UUID.randomUUID()
        val event = eventMockFactory.createRobotMovedEventMock(robotId, originPlanetId, destinationPlanetId)
        val expected = RobotMovedEvent(robotId, originPlanetId, destinationPlanetId)
        assertClassTypeAndContents(event, expected, RobotMovedEvent::class.java)
    }

    @Test
    fun test_robotAttackedEvent() {
        val attackingRobotId = UUID.randomUUID()
        val defendingRobotId = UUID.randomUUID()
        val event = eventMockFactory.createRobotAttackedEventMock(attackingRobotId, defendingRobotId)
        val expected = RobotAttackedEvent(attackingRobotId, defendingRobotId)
        assertClassTypeAndContents(event, expected, RobotAttackedEvent::class.java)
    }

    @Test
    fun test_robotKilledEvent() {
        val killedRobotId = UUID.randomUUID()
        val event = eventMockFactory.createRobotKilledEventMock(killedRobotId)
        val expected = RobotKilledEvent(killedRobotId)
        assertClassTypeAndContents(event, expected, RobotKilledEvent::class.java)
    }

    @Test
    fun test_robotHealthUpdated() {
        val amount = 0
        val newHealth = 0
        val event = eventMockFactory.createRobotHealthUpdatedEventMock(robotId, amount, newHealth)
        val expected = RobotHealthUpdatedEvent(robotId, amount, newHealth)
        assertClassTypeAndContents(event, expected, RobotHealthUpdatedEvent::class.java)
    }

    @Test
    fun test_robotEnergyUpdated() {
        val amount = 0
        val newEnergy = 0
        val event = eventMockFactory.createRobotEnergyUpdatedEventMock(robotId, amount, newEnergy)
        val expected = RobotEnergyUpdatedEvent(robotId, amount, newEnergy)
        assertClassTypeAndContents(event, expected, RobotEnergyUpdatedEvent::class.java)
    }

    @Test
    fun test_robotResourceMined() {
        val amount = 0
        val resource = ResourceType.GEM
        val event = eventMockFactory.createRobotResourceMinedEventMock(robotId, amount, resource.name.lowercase())
        val expected = RobotResourceMinedEvent(robotId, amount, resource)
        assertClassTypeAndContents(event, expected, RobotResourceMinedEvent::class.java)
    }

    @Test
    fun test_robotInventoryUpdated() {
        val coal = 1
        val iron = 2
        val gem = 3
        val gold = 4
        val platin = 5
        val event = eventMockFactory.createRobotInventoryUpdatedEventMock(
            robotId,
            MockRobotInventory(MockInventoryResources(coal, iron, gem, gold, platin), storageLevel, usedStorage, maxStorage, full)
        )
        val expected = RobotInventoryUpdatedEvent(robotId, Inventory(ResourceInventory(coal, iron, gem, gold, platin), storageLevel, usedStorage, maxStorage, full))
        assertClassTypeAndContents(event, expected, RobotInventoryUpdatedEvent::class.java)
    }

    @Test
    fun test_robotUpgraded() {
        val level = 2
        val upgradeType = UpgradeType.STORAGE
        val event = eventMockFactory.createRobotUpgradedEventMock(robotId, level, upgradeType.type)
        val expected = RobotUpgradedEvent(robotId, level, upgradeType)
        assertClassTypeAndContents(event, expected, RobotUpgradedEvent::class.java)
    }

    @Test
    fun test_exception() {
        assertThrows<MapperUnknownEventTypeException> {
            mapper.mapConsumerRecordToEvent("foobar", robotSpawnedEvent.timestamp, robotSpawnedEvent.payload)
        }
    }

    private fun assertClassTypeAndContents(event: EventMock, expected: InternalEvent, eventClass: Class<out InternalEvent>) {
        val result = mapper.mapConsumerRecordToEvent(event.type, event.timestamp, event.payload)
        assertThat(result.javaClass).isEqualTo(eventClass)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
    }

}