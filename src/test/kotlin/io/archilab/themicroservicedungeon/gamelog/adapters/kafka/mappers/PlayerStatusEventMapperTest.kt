package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers.PlayerStatusEventMapper
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.PlayerStatusEvent
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks.EventMockFactory
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.boot.test.context.SpringBootTest
import java.util.*

class PlayerStatusEventMapperTest : TestWithH2AndWithoutKafka() {

    private val eventMockFactory = EventMockFactory()
    private val mapper = PlayerStatusEventMapper()

    private val playerId = UUID.randomUUID()
    private val gameId = UUID.randomUUID()
    private val name = "foobar"
    private val kafkaEvent = eventMockFactory.createPlayerStatusEventMock(gameId, playerId, name)

    @Test
    fun test_mapConsumerRecordToEvent_result() {
        val expected = PlayerStatusEvent(gameId, playerId, name)
        val result = mapper.mapConsumerRecordToEvent(kafkaEvent.type, kafkaEvent.timestamp, kafkaEvent.payload)
        assertThat(result.javaClass).isEqualTo(PlayerStatusEvent::class.java)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun test_mapConsumerRecordToEvent_exception() {
        assertThrows<MapperUnknownEventTypeException> {
            mapper.mapConsumerRecordToEvent("foobar", kafkaEvent.timestamp, kafkaEvent.payload)
        }
    }

}