package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks.EventMockFactory
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RoundStatus
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.RoundStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.boot.test.context.SpringBootTest
import java.time.Instant
import java.util.*


internal class RoundStatusEventMapperTest : TestWithH2AndWithoutKafka() {

    private val eventMockFactory = EventMockFactory()
    private val mapper = RoundStatusEventMapper()

    @Test
    fun test_mapConsumerRecordToEvent_result() {
        val gameId = UUID.randomUUID()
        val roundId = UUID.randomUUID()
        val roundNumber = 1
        val roundStatus = RoundStatus.STARTED
        val event = eventMockFactory.createRoundStatusEventMock(gameId, roundId, roundNumber, roundStatus.status)
        val expected = RoundStatusEvent(gameId, roundId, roundNumber, roundStatus, Instant.parse(event.timestamp))
        val result = mapper.mapConsumerRecordToEvent(event.type, event.timestamp, event.payload)
        assertThat(result.javaClass).isEqualTo(RoundStatusEvent::class.java)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun test_roundStatusWithTimings() {
        val gameId = UUID.randomUUID()
        val roundId = UUID.randomUUID()
        val roundNumber = 1
        val roundStatus = RoundStatus.STARTED
        val timestamp = EventMockFactory.generateTimestampString()
        val event = eventMockFactory.createRoundStatusEventMockWithTimings(gameId, roundId, roundNumber, roundStatus.status, timestamp, timestamp, timestamp, timestamp, timestamp, timestamp)
        val expected = RoundStatusEvent(gameId, roundId, roundNumber, roundStatus, Instant.parse(event.timestamp))
        val result = mapper.mapConsumerRecordToEvent(event.type, event.timestamp, event.payload)
        assertThat(result.javaClass).isEqualTo(RoundStatusEvent::class.java)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun test_mapConsumerRecordToEvent_exception() {
        val invalidEventType = "foobar"
        val event = eventMockFactory.createRoundStatusEventMock(
            UUID.randomUUID(),
            UUID.randomUUID(),
            1,
            "started"
        )
        assertThrows<MapperUnknownEventTypeException> {
            mapper.mapConsumerRecordToEvent(invalidEventType,event.timestamp, event.payload)
        }
    }

}