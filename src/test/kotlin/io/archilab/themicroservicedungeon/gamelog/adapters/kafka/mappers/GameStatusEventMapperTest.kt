package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks.EventMockFactory
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.GameStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.Instant
import java.util.*


internal class GameStatusEventMapperTest : TestWithH2AndWithoutKafka() {

    private val eventMockFactory = EventMockFactory()
    private val mapper = GameStatusEventMapper()

    private val gameId = UUID.randomUUID()
    private val gameWorldId = UUID.randomUUID()
    private val status = GameStatus.CREATED
    private val kafkaEvent = eventMockFactory.createGameStatusEventMock(gameId, gameWorldId, status.status)

    @Test
    fun test_mapConsumerRecordToEvent_result() {
        val expected = GameStatusEvent(gameId, gameWorldId, status, Instant.parse(kafkaEvent.timestamp))
        val result = mapper.mapConsumerRecordToEvent(kafkaEvent.type, kafkaEvent.timestamp, kafkaEvent.payload)
        assertThat(result.javaClass).isEqualTo(GameStatusEvent::class.java)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
        println(result)
    }

    @Test
    fun test_mapConsumerRecordToEvent_withoutGameWorldId() {
        val event = eventMockFactory.createGameStatusEventMockWithoutGameWorldId(gameId, status.status)
        val expected = GameStatusEvent(gameId, null, status, Instant.parse(event.timestamp))
        val result = mapper.mapConsumerRecordToEvent(event.type, event.timestamp, event.payload)
        assertThat(result.javaClass).isEqualTo(GameStatusEvent::class.java)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
        println(result)
    }

    @Test
    fun test_mapConsumerRecordToEvent_exception() {
        assertThrows<MapperUnknownEventTypeException> {
            mapper.mapConsumerRecordToEvent("unknown-type", kafkaEvent.timestamp, kafkaEvent.payload)
        }
    }

}