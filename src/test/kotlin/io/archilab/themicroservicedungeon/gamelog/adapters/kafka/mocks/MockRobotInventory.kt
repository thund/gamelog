package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks

data class MockRobotInventory(
    val resources: MockInventoryResources,
    val storageLevel: Int,
    val usedStorage: Int,
    val maxStorage: Int,
    val full: Boolean
) {
}