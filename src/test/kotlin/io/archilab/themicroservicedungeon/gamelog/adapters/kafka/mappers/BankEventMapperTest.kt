package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks.EventMockFactory
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.BankAccountClearedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.BankAccountInitializedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.BankAccountTransactionBookedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.boot.test.context.SpringBootTest
import java.time.Instant
import java.util.*

class BankEventMapperTest : TestWithH2AndWithoutKafka() {

    private val eventMockFactory = EventMockFactory()
    private val mapper = BankEventMapper()

    private val playerId = UUID.randomUUID()

    private val balanceCleared = 0

    val bankAccountClearedEvent = eventMockFactory.createBankAccountClearedEventMock(playerId, balanceCleared)

    @Test
    fun test_bankAccountCleared() {
        val expected = BankAccountClearedEvent(playerId, balanceCleared, Instant.parse(bankAccountClearedEvent.timestamp))
        val result = mapper.mapConsumerRecordToEvent(bankAccountClearedEvent.type, bankAccountClearedEvent.timestamp, bankAccountClearedEvent.payload)
        assertThat(result.javaClass).isEqualTo(BankAccountClearedEvent::class.java)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun test_bankAccountInitialized() {
        val balance = 0
        val event = eventMockFactory.createBankAccountInitializedEventMock(playerId, balance)
        val expected = BankAccountInitializedEvent(playerId, balance, Instant.parse(event.timestamp))
        val result = mapper.mapConsumerRecordToEvent(event.type, event.timestamp, event.payload)
        assertThat(result.javaClass).isEqualTo(BankAccountInitializedEvent::class.java)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun test_bankAccountTransactionBooked() {
        val transactionAmount = 10
        val balance = 30
        val event = eventMockFactory.createBankAccountTransactionBookedEventMock(playerId, transactionAmount, balance)
        val expected = BankAccountTransactionBookedEvent(playerId, transactionAmount, balance, Instant.parse(event.timestamp))
        val result = mapper.mapConsumerRecordToEvent(event.type, event.timestamp, event.payload)
        assertThat(result.javaClass).isEqualTo(BankAccountTransactionBookedEvent::class.java)
        assertThat(result).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun test_exception() {
        assertThrows<MapperUnknownEventTypeException> {
            mapper.mapConsumerRecordToEvent("foobar", bankAccountClearedEvent.timestamp, bankAccountClearedEvent.payload)
        }
    }

}