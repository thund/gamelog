package io.archilab.themicroservicedungeon.gamelog.domain.trophies

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.entities.*
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.PlayerSuccess
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ScoreboardCategory
import io.archilab.themicroservicedungeon.gamelog.domain.enums.TrophyType
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.util.*

class TrophiesStrategyTest: TestWithH2AndWithoutKafka() {

    private val gameId = UUID.randomUUID()
    private val gameWorldId = null

    private val goodPlayer = "good"
    private val mediumPlayer = "medium"
    private val badPlayer = "bad"
    private val inactivePlayer = "inactive"

    @Autowired
    private lateinit var trophiesManager: TrophiesManager

    @Autowired
    private lateinit var trophiesStrategy: TrophiesStrategy

    @Test
    fun test_fighting() {
        testCategory(ScoreboardCategory.FIGHTING, false)
    }

    @Test
    fun test_mining() {
        testCategory(ScoreboardCategory.MINING, false)
    }

    @Test
    fun test_trading() {
        testCategory(ScoreboardCategory.TRADING, false)
    }

    @Test
    fun test_traveling() {
        testCategory(ScoreboardCategory.TRAVELING, false)
    }

    @Test
    fun test_game() {
        testCategory(ScoreboardCategory.GAME, false)
    }

    @Test
    fun test_sharingTrophies() {
        testCategory(ScoreboardCategory.FIGHTING, true)
        testCategory(ScoreboardCategory.MINING, true)
        testCategory(ScoreboardCategory.TRADING, true)
        testCategory(ScoreboardCategory.TRAVELING, true)
        testCategory(ScoreboardCategory.GAME, true)
    }

    fun testCategory(category: ScoreboardCategory, withDuplicateScores: Boolean) {
        val game = createGameIncludingPlayersAndScoreboard(withDuplicateScores)

        trophiesStrategy.awardTrophiesForGame(game)

        val badPlayerTrophy = when(category) {
            ScoreboardCategory.FIGHTING -> TrophyType.FightingThirdPlace
            ScoreboardCategory.GAME -> TrophyType.GameThirdPlace
            ScoreboardCategory.MINING -> TrophyType.MiningThirdPlace
            ScoreboardCategory.TRADING -> TrophyType.TradingThirdPlace
            ScoreboardCategory.TRAVELING -> TrophyType.TravelingThirdPlace
        }

        val mediumPlayerTrophy = when(category) {
            ScoreboardCategory.FIGHTING -> TrophyType.FightingSecondPlace
            ScoreboardCategory.GAME -> TrophyType.GameSecondPlace
            ScoreboardCategory.MINING -> TrophyType.MiningSecondPlace
            ScoreboardCategory.TRADING -> TrophyType.TradingSecondPlace
            ScoreboardCategory.TRAVELING -> TrophyType.TravelingSecondPlace
        }

        val goodPlayerTrophy = when(category) {
            ScoreboardCategory.FIGHTING -> TrophyType.FightingFirstPlace
            ScoreboardCategory.GAME -> TrophyType.GameFirstPlace
            ScoreboardCategory.MINING -> TrophyType.MiningFirstPlace
            ScoreboardCategory.TRADING -> TrophyType.TradingFirstPlace
            ScoreboardCategory.TRAVELING -> TrophyType.TravelingFirstPlace
        }

        assertHasTrophies(game, inactivePlayer, listOf())
        assertHasTrophies(game, badPlayer, listOf(badPlayerTrophy))
        assertHasTrophies(game, mediumPlayer, listOf(mediumPlayerTrophy))
        assertHasTrophies(game, goodPlayer, listOf(goodPlayerTrophy))
    }

    private fun assertHasTrophies(game: Game, playerName: String, earnedTrophies: List<TrophyType>) {
        game.players.filter { player -> player.name.contains(playerName) }.forEach {player ->
            if(earnedTrophies.isEmpty()) {
                assertThat(player.trophies).isEmpty()
            } else {
                earnedTrophies.forEach {trophyType ->
                    val trophy = trophiesManager.getTrophy(trophyType)!!
                    assertThat(player.trophies).usingRecursiveFieldByFieldElementComparator().contains(
                        PlayerTrophy(game.id, trophy)
                    )
                }
            }
        }
    }

    private fun createGameIncludingPlayersAndScoreboard(withDuplicateScores: Boolean): Game {
        val game = Game(gameId, gameWorldId, GameStatus.ENDED)
        val players = mutableListOf(
            Pair(PlayerSuccess.HIGH, Player(UUID.randomUUID(), goodPlayer)),
            Pair(PlayerSuccess.MEDIUM, Player(UUID.randomUUID(), mediumPlayer)),
            Pair(PlayerSuccess.LOW, Player(UUID.randomUUID(), badPlayer)),
            Pair(null, Player(UUID.randomUUID(), inactivePlayer))
        )
        if (withDuplicateScores) {
            duplicatePlayers(players)
        }
        players.forEach {
            it.second.joinGame(game)
        }
        game.scoreboard = createScoreboard(game, players)
        return game
    }

    private fun duplicatePlayers(players: MutableList<Pair<PlayerSuccess?, Player>>) {
        val playerCount = players.count()
        for (i in 0 until playerCount) {
            val element = players[i]
            players.add(Pair(element.first, Player(UUID.randomUUID(), element.second.name + "2")))
        }
    }

    private fun createScoreboard(
        game: Game,
        players: List<Pair<PlayerSuccess?, Player>>
    ) = Scoreboard(
        game.id,
        100,
        players.map {
            val value = when (it.first) {
                PlayerSuccess.HIGH -> 10
                PlayerSuccess.MEDIUM -> 5
                PlayerSuccess.LOW -> 1
                null -> 0
            }
            PlayerScore(
                it.second,
                value,
                value,
                value,
                value,
                value
            )
        }
    )

}