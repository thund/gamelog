package io.archilab.themicroservicedungeon.gamelog.domain.broker

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Inventory
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Movement
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Robot
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.UpgradeType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.WorldStatus
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.GameStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.PlayerStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.GameWorldCreatedEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.embedded.GameWorldPlanet
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.*
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.legacy.*
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlayerRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.RobotRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Transactional
class InternalEventListenerTestForRobotDomain : TestWithH2AndWithoutKafka() {

    @Autowired
    private lateinit var internalEventListener: InternalEventListener

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var robotRepository: RobotRepository

    private val gameId = UUID.randomUUID()
    private val gameWorldId = UUID.randomUUID()
    private val playerId = UUID.randomUUID()
    private val playerName = "foobar"

    private val robotId = UUID.randomUUID()
    private val alive = true
    private val maxHealth = 100
    private val maxEnergy = 60
    private val energyRegeneration = 8
    private val attackDamage = 5
    private val miningSpeed = 10
    private val health = 75
    private val energy = 45
    private val healthLevel = 1
    private val damageLevel = 1
    private val miningSpeedLevel = 1
    private val miningLevel = 1
    private val energyLevel = 1
    private val energyRegenerationLevel = 1

    private val usedStorage = 50
    private val maxStorage = 100
    private val full = false
    private val storageLevel = 1
    private val coal = 10
    private val iron = 10
    private val gem = 10
    private val gold = 10
    private val platin = 10

    private val planet1Id = UUID.randomUUID()
    private val planet2Id = UUID.randomUUID()
    private val movementDifficulty = 1
    private val resourceType = ResourceType.COAL
    private val planet1 = GameWorldPlanet(planet1Id, 0, 0, 10, null)
    private val planet2 = GameWorldPlanet(planet2Id, 0, 1, 10, null)

    // For fighting.
    private val attackingPlayerId = UUID.randomUUID()
    private val defendingPlayerId = UUID.randomUUID()
    private val attackingRobotId = UUID.randomUUID()
    private val defendingRobotId = UUID.randomUUID()

    private val robotProperties = createRobotProperties(robotId, playerId)

    private val robotSpawnedEvent = RobotSpawnedEvent(robotProperties)
    private val gameCreatedEvent = GameStatusEvent(gameId, gameWorldId, GameStatus.CREATED, Instant.now())
    private val playerJoinedEvent = PlayerStatusEvent(gameId, playerId, playerName)

    @Test
    fun test_robotSpawnedEvent() {
        prepareGame()

        internalEventListener.listenForInternalEvents(robotSpawnedEvent)

        val expected = Robot(robotId, alive, playerId, maxHealth, maxEnergy, energyRegeneration, attackDamage, miningSpeed, health, energy, healthLevel, damageLevel, miningSpeedLevel, miningLevel, energyLevel, energyRegenerationLevel, storageLevel).apply {
            spawn(planet1Id)
            inventory = Inventory(coal, iron, gem, gold, platin)
            gameId = this@InternalEventListenerTestForRobotDomain.gameId
        }
        val result = playerRepository.getPlayerById(playerId)
        assertThat(result).isNotNull
        assertThat(result?.robots?.first()).usingRecursiveComparison().isEqualTo(expected)
    }

    @Test
    fun test_robotKilledEvent() {
        prepareGameAndSpawnRobot()
        val robotKilledEvent = RobotKilledEvent(robotId)

        internalEventListener.listenForInternalEvents(robotKilledEvent)

        val playerResult = playerRepository.getPlayerById(playerId)
        assertThat(playerResult).isNotNull
        playerResult?.run {
            assertThat(robots.toTypedArray()[0].alive).isFalse
        }
    }

    @Test
    fun test_robotEnergyUpdatedEvent() {
        prepareGameAndSpawnRobot()
        val amount = -10
        val newEnergyLevel = energy + amount
        val robotEnergyUpdatedEvent = RobotEnergyUpdatedEvent(robotId, amount, newEnergyLevel)

        internalEventListener.listenForInternalEvents(robotEnergyUpdatedEvent)

        val player = playerRepository.getPlayerById(playerId)
        assertThat(player).isNotNull
        player?.run {
            assertThat(robots.toTypedArray()[0].energy).isEqualTo(newEnergyLevel)
        }
    }

    @Test
    fun test_robotHealthUpdatedEvent() {
        prepareGameAndSpawnRobot()
        val amount = -10
        val newHealth = health + amount
        val robotHealthUpdatedEvent = RobotHealthUpdatedEvent(robotId, amount, newHealth)

        internalEventListener.listenForInternalEvents(robotHealthUpdatedEvent)

        val player = playerRepository.getPlayerById(playerId)
        assertThat(player).isNotNull
        player?.run {
            assertThat(player.robots.toTypedArray()[0].health).isEqualTo(newHealth)
        }
    }

    @Test
    fun test_robotInventoryUpdatedEvent() {
        prepareGameAndSpawnRobot()
        val coal = 10
        val iron = 10
        val gem = 10
        val gold = 10
        val platin = 10
        val inventory = Inventory(ResourceInventory(coal, iron, gem, gold, platin), storageLevel, usedStorage, maxStorage, full)
        val robotInventoryUpdatedEvent = RobotInventoryUpdatedEvent(robotId, inventory)

        internalEventListener.listenForInternalEvents(robotInventoryUpdatedEvent)

        val player = playerRepository.getPlayerById(playerId)
        val expected = Inventory(coal, iron, gem, gold, platin)
        assertThat(player).isNotNull
        player?.run {
            assertThat(robots.toTypedArray()[0].inventory).usingRecursiveComparison().isEqualTo(expected)
        }
    }

    @Test
    fun test_robotMovedEvent() {
        prepareGameAndSpawnRobot()
        prepareGameWorld()
        val robotMovedEvent = RobotMovedEvent(robotId, planet1Id, planet2Id)

        internalEventListener.listenForInternalEvents(robotMovedEvent)

        val player = playerRepository.getPlayerById(playerId)
        assertThat(player).isNotNull
        player?.run {
            val robot = robots.toTypedArray()[0]
            assertThat(robot.movements).isNotEmpty
            assertThat(robot.movements[0]).usingRecursiveComparison().ignoringFields("id").isEqualTo(Movement(robotId, planet1Id, planet2Id))
            assertThat(robot.previousPlanetId).isEqualTo(planet1Id)
            assertThat(robot.currentPlanetId).isEqualTo(planet2Id)
        }

    }

    @Test
    fun test_robotResourceMinedEvent() {
        prepareGameAndSpawnRobot()
        prepareGameWorld()
        val amounts = mapOf(
            Pair(ResourceType.COAL, 1),
            Pair(ResourceType.IRON, 2),
            Pair(ResourceType.GEM, 3),
            Pair(ResourceType.GOLD, 4),
            Pair(ResourceType.PLATIN, 5)
        )

        amounts.forEach{
            internalEventListener.listenForInternalEvents(RobotResourceMinedEvent(robotId, it.value, it.key))
        }

        val robot = robotRepository.getRobotById(robotId)
        assertThat(robot).isNotNull
        robot?.run {
            assertThat(minedResources.coal).isEqualTo(amounts[ResourceType.COAL])
            assertThat(minedResources.iron).isEqualTo(amounts[ResourceType.IRON])
            assertThat(minedResources.gem).isEqualTo(amounts[ResourceType.GEM])
            assertThat(minedResources.gold).isEqualTo(amounts[ResourceType.GOLD])
            assertThat(minedResources.platin).isEqualTo(amounts[ResourceType.PLATIN])
        }
    }

    @Test
    fun test_robotAttackedEvent() {
        prepareGameForFighting(attackingPlayerId, defendingPlayerId, attackingRobotId, defendingRobotId)
        val robotAttackedEvent = RobotAttackedEvent(attackingRobotId, defendingRobotId)

        internalEventListener.listenForInternalEvents(robotAttackedEvent)

        val attackingRobot = robotRepository.getRobotById(attackingRobotId)
        val defendingRobot = robotRepository.getRobotById(defendingRobotId)
        assertThat(attackingRobot).isNotNull
        assertThat(defendingRobot).isNotNull
        attackingRobot?.run {
            assertThat(fights).isNotEmpty
            fights.firstOrNull()?.let {fight ->
                assertThat(fight.attackingRobotId).isEqualTo(this.robotId)
                assertThat(fight.defendingRobotId).isEqualTo(defendingRobotId)
                assertThat(offensiveFights.first()).isEqualTo(fight)
                assertThat(defensiveFights).isEmpty()
            }
        }
        defendingRobot?.run {
            assertThat(fights).isNotEmpty
            fights.firstOrNull()?.let {fight ->
                assertThat(fight.attackingRobotId).isEqualTo(attackingRobotId)
                assertThat(fight.defendingRobotId).isEqualTo(this.robotId)
                assertThat(offensiveFights).isEmpty()
                assertThat(defensiveFights.first()).isEqualTo(fight)
            }
        }
    }

    @Test
    fun test_robotUpgradedEvent_storage() {
        runTestOfRobotUpgradedEventForConfiguration(1, UpgradeType.STORAGE)
    }

    @Test
    fun test_robotUpgradedEvent_health() {
        runTestOfRobotUpgradedEventForConfiguration(1, UpgradeType.HEALTH)
    }

    @Test
    fun test_robotUpgradedEvent_damage() {
        runTestOfRobotUpgradedEventForConfiguration(1, UpgradeType.DAMAGE)
    }

    @Test
    fun test_robotUpgradedEvent_miningSpeed() {
        runTestOfRobotUpgradedEventForConfiguration(1, UpgradeType.MINING_SPEED)
    }

    @Test
    fun test_robotUpgradedEvent_mining() {
        runTestOfRobotUpgradedEventForConfiguration(1, UpgradeType.MINING)
    }

    @Test
    fun test_robotUpgradedEvent_energy() {
        runTestOfRobotUpgradedEventForConfiguration(1, UpgradeType.MAXIMUM_ENERGY)
    }

    @Test
    fun test_robotUpgradedEvent_energyRegeneration() {
        runTestOfRobotUpgradedEventForConfiguration(1, UpgradeType.ENERGY_REGENERATION)
    }

    private fun runTestOfRobotUpgradedEventForConfiguration(upgradeStep: Int, upgradeType: UpgradeType) {
        prepareGameAndSpawnRobot()
        val robot = robotRepository.getRobotById(robotId)!!
        val event = prepareRobotUpgradeEvent(robot, upgradeStep, upgradeType)
        val expected = prepareUpgradedRobot(robot, event.level, event.upgrade)

        internalEventListener.listenForInternalEvents(event)

        val upgradedRobot = robotRepository.getRobotById(robotId)!!
        assertThat(upgradedRobot).usingRecursiveComparison().isEqualTo(expected)
    }

    fun prepareRobotUpgradeEvent(robot: Robot, upgradeStep: Int, upgradeType: UpgradeType): RobotUpgradedEvent {
        val newLevel = with(robot) {
            when(upgradeType) {
                UpgradeType.STORAGE -> storageLevel + upgradeStep
                UpgradeType.HEALTH -> healthLevel + upgradeStep
                UpgradeType.DAMAGE -> damageLevel + upgradeStep
                UpgradeType.MINING_SPEED -> miningSpeedLevel + upgradeStep
                UpgradeType.MINING -> miningLevel + upgradeStep
                UpgradeType.MAXIMUM_ENERGY -> energyLevel + upgradeStep
                UpgradeType.ENERGY_REGENERATION -> energyRegenerationLevel + upgradeStep
            }
        }
        return RobotUpgradedEvent(robotId, newLevel, upgradeType)
    }

    private fun prepareUpgradedRobot(robot: Robot, level: Int, upgrade: UpgradeType): Robot {
        return with(robot) {
            val storageLevel = if (upgrade == UpgradeType.STORAGE) level else storageLevel
            val healthLevel = if (upgrade == UpgradeType.HEALTH) level else healthLevel
            val damageLevel = if (upgrade == UpgradeType.DAMAGE) level else damageLevel
            val miningSpeedLevel = if (upgrade == UpgradeType.MINING_SPEED) level else miningSpeedLevel
            val miningLevel = if (upgrade == UpgradeType.MINING) level else miningLevel
            val energyLevel = if (upgrade == UpgradeType.MAXIMUM_ENERGY) level else energyLevel
            val energyRegenerationLevel =
                if (upgrade == UpgradeType.ENERGY_REGENERATION) level else energyRegenerationLevel
            Robot(
                robotId,
                alive,
                playerId,
                maxHealth,
                maxEnergy,
                energyRegeneration,
                attackDamage,
                miningSpeed,
                health,
                energy,
                healthLevel,
                damageLevel,
                miningSpeedLevel,
                miningLevel,
                energyLevel,
                energyRegenerationLevel,
                storageLevel
            ).apply {
                inventory = this@with.inventory
                spawn(this@with.currentPlanetId!!)
                gameId = this@InternalEventListenerTestForRobotDomain.gameId
            }
        }
    }

    private fun prepareGameForFighting(
        attackingPlayerId: UUID,
        defendingPlayerId: UUID,
        attackingRobotId: UUID,
        defendingRobotId: UUID
    ) {
        val attackingPlayerJoinEvent = PlayerStatusEvent(gameId, attackingPlayerId, "attacking")
        val defendingPlayerJoinEvent = PlayerStatusEvent(gameId, defendingPlayerId, "defending")
        val attackingRobotSpawnedEvent = RobotSpawnedEvent(createRobotProperties(attackingRobotId, attackingPlayerId))
        val defendingRobotSpawnedEvent = RobotSpawnedEvent(createRobotProperties(defendingRobotId, defendingPlayerId))

        internalEventListener.listenForInternalEvents(gameCreatedEvent)
        internalEventListener.listenForInternalEvents(attackingPlayerJoinEvent)
        internalEventListener.listenForInternalEvents(defendingPlayerJoinEvent)
        internalEventListener.listenForInternalEvents(attackingRobotSpawnedEvent)
        internalEventListener.listenForInternalEvents(defendingRobotSpawnedEvent)
    }

    private fun prepareGameAndSpawnRobot() {
        prepareGame()
        internalEventListener.listenForInternalEvents(robotSpawnedEvent)
    }

    private fun prepareGame() {
        internalEventListener.listenForInternalEvents(gameCreatedEvent)
        internalEventListener.listenForInternalEvents(playerJoinedEvent)
    }

    private fun prepareGameWorld() {
        val gameWorldId = UUID.randomUUID()
        val planets = listOf(
            planet1,
            planet2
        )
        val gameWorldCreatedEvent = GameWorldCreatedEvent(gameWorldId, WorldStatus.ACTIVE, planets)
        internalEventListener.listenForInternalEvents(gameWorldCreatedEvent)
    }

    private fun createRobotProperties(robotId: UUID, playerId: UUID) : RobotProperties {
        return RobotProperties(
            robotId,
            alive,
            playerId,
            maxHealth,
            maxEnergy,
            energyRegeneration,
            attackDamage,
            miningSpeed,
            health,
            energy,
            healthLevel,
            damageLevel,
            miningSpeedLevel,
            miningLevel,
            energyLevel,
            energyRegenerationLevel,
            Planet(
                planet1Id,
                gameWorldId,
                movementDifficulty,
                resourceType.name
            ),
            Inventory(
                ResourceInventory(coal, iron, gem, gold, platin),
                storageLevel, usedStorage, maxStorage, full
            )
        )
    }
}