package io.archilab.themicroservicedungeon.gamelog.domain.scoreboard

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Player
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Scoreboard
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RoundStatus
import io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers.GameEventHandler
import io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers.RoundStatusEventHandler
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.GameStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.RoundStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.GameRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlayerRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Transactional
class ScoreboardCalculationTest : TestWithH2AndWithoutKafka() {

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var gameEventHandler: GameEventHandler

    @Autowired
    private lateinit var roundStatusEventHandler: RoundStatusEventHandler

    private val gameId = UUID.randomUUID()
    private val gameWorldId = null

    private val roundId = UUID.randomUUID()
    private val roundNumber = 1

    @Test
    fun testCalculationOfScoreboard_startOfGame() {
        val game = createGame()
        createAndRegisterPlayer(game)
        startRound()

        assertThat(gameRepository.getGameById(gameId)!!.scoreboard)
            .usingRecursiveComparison()
            .isEqualTo(
                Scoreboard(gameId, 0, listOf())
            )

    }

    @Test
    fun testCalculationOfScoreboard_calculationOnRoundEnd() {
        val game = createGame()
        createAndRegisterPlayer(game)

        startRound()
        endRound()

        gameRepository.getGameById(gameId)!!.scoreboard.also {
            assertThat(it.gameId).isEqualTo(gameId)
            assertThat(it.roundNumber).isEqualTo(roundNumber)
            assertThat(it.playerScores).isNotEmpty
        }

    }

    private fun createGame() : Game {
        gameEventHandler.handleGameStatusEvent(GameStatusEvent(gameId, gameWorldId, GameStatus.CREATED, Instant.now()))
        return gameRepository.getGameById(gameId)!!
    }

    private fun createAndRegisterPlayer(game: Game): Player {
        val player = Player(UUID.randomUUID(), "player_")
        playerRepository.savePlayer(player)
        player.joinGame(game)
        gameRepository.saveGame(game)
        playerRepository.savePlayer(player)
        return playerRepository.getPlayerById(player.id)!!
    }

    private fun startRound() {
        roundStatusEventHandler.handleRoundStatusEvent(
            RoundStatusEvent(
                gameId,
                roundId,
                roundNumber,
                RoundStatus.STARTED,
                Instant.now()
            )
        )
    }

    private fun endRound() {
        roundStatusEventHandler.handleRoundStatusEvent(RoundStatusEvent(gameId, roundId, roundNumber, RoundStatus.ENDED, Instant.now()))
    }

}