package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mocks.EventMockFactory
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.GameStatusEvent
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Transactional
class GameRepositoryTest : TestWithH2AndWithoutKafka() {

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Test
    fun testPersistingGame() {
        val gameId = UUID.randomUUID()
        val gameWorldId = UUID.randomUUID()
        val gameCreatedEvent = GameStatusEvent(gameId, gameWorldId, GameStatus.CREATED, Instant.parse(EventMockFactory.generateTimestampString()))
        val game = Game(gameCreatedEvent.gameId, gameWorldId, gameCreatedEvent.status)
        gameRepository.saveGame(game)
        val savedGame = gameRepository.getGameById(gameId)
        assertThat(savedGame).usingRecursiveComparison().isEqualTo(game)
    }

}