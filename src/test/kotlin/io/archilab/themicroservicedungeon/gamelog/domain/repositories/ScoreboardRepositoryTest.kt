package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import TestWithH2AndWithoutKafka
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Player
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.PlayerSuccess
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RoundStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ScoreboardCategory
import io.archilab.themicroservicedungeon.gamelog.domain.factories.PreconfiguredPlayersFactory
import io.archilab.themicroservicedungeon.gamelog.domain.scoreboard.ScoreboardStrategy
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import java.time.Instant
import java.util.*

@Transactional
class ScoreboardRepositoryTest : TestWithH2AndWithoutKafka() {

    @Autowired
    private lateinit var scoreboardRepository: ScoreboardRepository

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var scoreboardStrategy: ScoreboardStrategy

    private val preconfiguredPlayersFactory = PreconfiguredPlayersFactory()

    private val gameId = UUID.randomUUID()
    private val roundId = UUID.randomUUID()
    private val roundNumber = 42

    @Test
    fun test_getScoreboard() {
        val gameStatus = GameStatus.STARTED
        val game = createGame(gameId, null, gameStatus)
        val goodPlayer = preconfiguredPlayersFactory.createFocusedPlayer(ScoreboardCategory.GAME, PlayerSuccess.HIGH).also {
            registerPlayerForGame(it, game)
        }
        val mediumPlayer = preconfiguredPlayersFactory.createFocusedPlayer(ScoreboardCategory.GAME, PlayerSuccess.MEDIUM).also {
            registerPlayerForGame(it, game)
        }
        val badPlayer = preconfiguredPlayersFactory.createFocusedPlayer(ScoreboardCategory.GAME, PlayerSuccess.LOW).also {
            registerPlayerForGame(it, game)
        }
        startGame(game)

        val scoreboard = scoreboardRepository.getScoreboard()

        assertThat(scoreboard?.gameId).isEqualTo(gameId)
        assertThat(scoreboard?.gameStatus).isEqualTo(gameStatus)
        assertThat(scoreboard?.roundNumber).isEqualTo(roundNumber)
        assertThat(scoreboard?.scoreboardEntries?.size).isEqualTo(3)
        assertThat(scoreboard?.scoreboardEntries?.map { it.player.id }).isEqualTo(listOf(goodPlayer.id, mediumPlayer.id, badPlayer.id))
    }

    private fun createGame(gameId: UUID, gameWorldId: UUID?, gameStatus: GameStatus) : Game {
        val game = Game(gameId, gameWorldId, gameStatus)
        gameRepository.saveGame(game)
        return game
    }

    private fun registerPlayerForGame(player: Player, game: Game) {
        playerRepository.savePlayer(player)
        player.joinGame(game)
        playerRepository.savePlayer(player)
        gameRepository.saveGame(game)
    }

    private fun startGame(game: Game) {
        game.startGame(Instant.now())
        game.newRound(roundId, roundNumber, Instant.now())
        game.updateRound(roundId, RoundStatus.ENDED, Instant.now())
        game.scoreboard = scoreboardStrategy.calculateScoreboard(game, roundNumber)
        gameRepository.saveGame(game)
    }

}