import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles

@SpringBootTest
@ActiveProfiles("no-kafka", "h2")
class TestWithH2AndWithoutKafka {
}