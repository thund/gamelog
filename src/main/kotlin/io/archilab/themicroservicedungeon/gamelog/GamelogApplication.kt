package io.archilab.themicroservicedungeon.gamelog

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GamelogApplication

fun main(args: Array<String>) {
    runApplication<GamelogApplication>(*args)
}
