package io.archilab.themicroservicedungeon.gamelog.domain.events.trading

import io.archilab.themicroservicedungeon.gamelog.domain.enums.TradableType
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import java.math.BigDecimal
import java.util.UUID

data class TradableBoughtEvent (
    val playerId: UUID,
    val robotId: UUID?,
    val type: TradableType,
    val name: String,
    val amount: Int,
    val pricePerUnit: BigDecimal,
    val totalPrice: BigDecimal
) : InternalEvent