package io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Price
import io.archilab.themicroservicedungeon.gamelog.domain.entities.PricesList
import io.archilab.themicroservicedungeon.gamelog.domain.enums.BankAccountTransactionType
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.*
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlayerRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PriceRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.time.Instant

@Component
class TradingEventHandler {

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var priceRepository: PriceRepository

    @Transactional
    fun handleBankAccountInitializedEvent(event: BankAccountInitializedEvent) {
        val player = playerRepository.getPlayerById(event.playerId)
        player?.apply {
            bookBankAccountTransaction(BankAccountTransactionType.INITIALIZED, 0, event.balance.toLong(), event.timestamp)
            playerRepository.savePlayer(this)
        }?: println("Can't initialize bank account. Player ${event.playerId} doesn't exist.")
    }

    @Transactional
    fun handleBankAccountClearedEvent(event: BankAccountClearedEvent) {
        val player = playerRepository.getPlayerById(event.playerId)
        player?.apply {
            bookBankAccountTransaction(BankAccountTransactionType.CLEARED, 0 - bankAccountBalance, event.balance.toLong(), event.timestamp)
            playerRepository.savePlayer(this)
        }?: println("Can't clear bank account. Player ${event.playerId} doesn't exist.")
    }

    @Transactional
    fun handleBankAccountTransactionBookedEvent(event: BankAccountTransactionBookedEvent) {
        val player = playerRepository.getPlayerById(event.playerId)
        player?.apply {
            bookBankAccountTransaction(BankAccountTransactionType.TRADE, event.transactionAmount.toLong(), event.balance.toLong(), event.timestamp)
            playerRepository.savePlayer(this)
        }?: println("Can't clear bank account. Player ${event.playerId} doesn't exist.")
    }

    @Transactional
    fun handleTradableBoughtEvent(event: TradableBoughtEvent) {
        val player = playerRepository.getPlayerById(event.playerId)
        player?.apply {
            bookBuyingTransaction(event.robotId, event.type, event.name, event.amount, event.pricePerUnit, event.totalPrice)
            playerRepository.savePlayer(this)
        }?: println("Can't book buying trade event. Player ${event.playerId} doesn't exist.")
    }

    @Transactional
    fun handleTradableSoldEvent(event: TradableSoldEvent) {
        val player = playerRepository.getPlayerById(event.playerId)
        player?.apply {
            bookSellingTransaction(event.robotId, event.type, event.name, event.amount, event.pricePerUnit, event.totalPrice)
            playerRepository.savePlayer(this)
        }?: println("Can't book selling trade event. Player ${event.playerId} doesn't exist.")
    }

    @Transactional
    fun handleTradablePricesEvent(event: TradablePricesEvent) {
        val pricesList = PricesList()
            .apply {
                timestamp = Instant.now()
                event.forEach {
                    prices.add(Price(it.name, it.price, it.type))
                }
            }
        priceRepository.savePricesList(pricesList)
    }

}