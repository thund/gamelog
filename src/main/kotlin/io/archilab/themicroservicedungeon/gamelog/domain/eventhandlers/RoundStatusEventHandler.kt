package io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers

import io.archilab.themicroservicedungeon.gamelog.domain.achievements.AchievementsStrategy
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Game
import io.archilab.themicroservicedungeon.gamelog.domain.enums.RoundStatus
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.RoundStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.GameRepository
import io.archilab.themicroservicedungeon.gamelog.domain.scoreboard.ScoreboardStrategy
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class RoundStatusEventHandler {

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Autowired
    private lateinit var scoreboardStrategy: ScoreboardStrategy

    @Autowired
    private lateinit var achievementsStrategy: AchievementsStrategy

    fun handleRoundStatusEvent(event: RoundStatusEvent) {
        when (event.roundStatus) {
            RoundStatus.STARTED -> handleRoundStarted(event)
            RoundStatus.COMMAND_INPUT_ENDED -> handleCommandInputEnded(event)
            RoundStatus.ENDED -> handleRoundEndedEvent(event)
        }
    }

    @Transactional
    fun handleRoundStarted(event: RoundStatusEvent) {
        val game = gameRepository.getGameById(event.gameId)
        game?.run {
            with(event) {
                game.newRound(roundId, roundNumber, timestamp)
                gameRepository.saveGame(game)
            }
        }
    }

    @Transactional
    fun handleCommandInputEnded(event: RoundStatusEvent) {
        updateRound(event)
    }

    @Transactional
    fun handleRoundEndedEvent(event: RoundStatusEvent) {
        updateRound(event)
    }

    private fun updateRound(event: RoundStatusEvent) {
        val game = gameRepository.getGameById(event.gameId)
        game?.run {
            updateRound(event.roundId, event.roundStatus, event.timestamp)
            if (event.roundStatus == RoundStatus.ENDED) {
                updateScoreboard(game, event.roundNumber)
                awardAchievements(game, event.roundNumber)
            }
            gameRepository.saveGame(this)
        }
    }

    private fun updateScoreboard(game: Game, roundNumber: Int) {
        val newScoreboard = scoreboardStrategy.calculateScoreboard(game, roundNumber)
        game.scoreboard = newScoreboard
    }

    private fun awardAchievements(game: Game, roundNumber: Int) {
        print("Awarding achievements after round $roundNumber")
        game.players.forEach {
            player -> achievementsStrategy.awardAchievementsToPlayer(game.id, player)
        }
    }

}