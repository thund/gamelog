package io.archilab.themicroservicedungeon.gamelog.domain.entities

import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class MinedResources() {
    @Column(name = "mined_coal")
    var coal: Int = 0

    @Column(name = "mined_iron")
    var iron: Int = 0

    @Column(name = "mined_gem")
    var gem: Int = 0

    @Column(name = "mined_gold")
    var gold: Int = 0

    @Column(name = "mined_platin")
    var platin: Int = 0


    constructor(coal: Int, iron: Int, gem: Int, gold: Int, platinum: Int) : this() {
        this.coal = coal
        this.iron = iron
        this.gem = gem
        this.gold = gold
        this.platin = platinum
    }
}