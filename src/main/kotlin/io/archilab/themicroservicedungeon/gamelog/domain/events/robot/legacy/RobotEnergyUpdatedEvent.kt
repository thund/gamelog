package io.archilab.themicroservicedungeon.gamelog.domain.events.robot.legacy

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import java.util.*

data class RobotEnergyUpdatedEvent(
    @JsonProperty("robot")
    val robotId: UUID,
    val amount: Int,
    val energy: Int
) : InternalEvent
