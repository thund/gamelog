package io.archilab.themicroservicedungeon.gamelog.domain.exceptions

class MapperUnknownEventTypeException(message: String = "") : Exception(message) {}
