package io.archilab.themicroservicedungeon.gamelog.domain.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.ScoreboardTrophiesDto

interface ScoreboardTrophiesRepository {

    fun getScoreboardTrophies() : ScoreboardTrophiesDto

}