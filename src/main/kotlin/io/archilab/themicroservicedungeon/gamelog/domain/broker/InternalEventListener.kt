package io.archilab.themicroservicedungeon.gamelog.domain.broker

import RobotSpawnedIntegrationEvent
import io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers.*
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.GameStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.PlayerStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.game.RoundStatusEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.map.*
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.*
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.legacy.*
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
@Transactional
class InternalEventListener {

    @Autowired
    private lateinit var gameEventHandler: GameEventHandler

    @Autowired
    private lateinit var playerStatusEventHandler: PlayerStatusEventHandler

    @Autowired
    private lateinit var roundStatusEventHandler: RoundStatusEventHandler

    @Autowired
    private lateinit var gameWorldEventHandler: GameWorldEventHandler

    @Autowired
    private lateinit var robotEventHandler: RobotEventHandler

    @Autowired
    private lateinit var robotIntegrationEventHandler: RobotIntegrationEventHandler

    @Autowired
    private lateinit var tradingEventHandler: TradingEventHandler

    @EventListener
    fun listenForInternalEvents(event: InternalEvent) {
        when(event) {
            is GameStatusEvent -> gameEventHandler.handleGameStatusEvent(event)
            is PlayerStatusEvent -> playerStatusEventHandler.handlePlayerStatusEvent(event)
            is RoundStatusEvent -> roundStatusEventHandler.handleRoundStatusEvent(event)
            is GameWorldCreatedEvent -> gameWorldEventHandler.handleGameWorldCreatedEvent(event)
            is GameWorldDeletedEvent -> gameWorldEventHandler.handleGameWorldDeletedEvent(event)
            is GameWorldStatusChangedEvent -> gameWorldEventHandler.handleGameWorldStatusChangedEvent(event)
            is ResourceMinedEvent -> gameWorldEventHandler.handleResourceMinedEvent(event)
            is PlanetDiscoveredEvent -> {} // No use for this information. Discard it.
            is RobotSpawnedEvent -> robotEventHandler.handleRobotSpawnedEvent(event)
            is RobotKilledEvent -> robotEventHandler.handleRobotKilledEvent(event)
            is RobotEnergyUpdatedEvent -> robotEventHandler.handleRobotEnergyUpdatedEvent(event)
            is RobotHealthUpdatedEvent -> robotEventHandler.handleRobotHealthUpdatedEvent(event)
            is RobotInventoryUpdatedEvent -> robotEventHandler.handleRobotInventoryUpdatedEvent(event)
            is RobotMovedEvent -> robotEventHandler.handleRobotMovedEvent(event)
            is RobotResourceMinedEvent -> robotEventHandler.handleRobotResourceMinedEvent(event)
            is RobotAttackedEvent -> robotEventHandler.handleRobotAttackedEvent(event)
            is RobotUpgradedEvent -> robotEventHandler.handleRobotUpgradedEvent(event)
            is BankAccountInitializedEvent -> tradingEventHandler.handleBankAccountInitializedEvent(event)
            is BankAccountClearedEvent -> tradingEventHandler.handleBankAccountClearedEvent(event)
            is BankAccountTransactionBookedEvent -> tradingEventHandler.handleBankAccountTransactionBookedEvent(event)
            is TradablePricesEvent -> tradingEventHandler.handleTradablePricesEvent(event)
            is TradableBoughtEvent -> tradingEventHandler.handleTradableBoughtEvent(event)
            is TradableSoldEvent -> tradingEventHandler.handleTradableSoldEvent(event)
            is RobotSpawnedIntegrationEvent -> robotIntegrationEventHandler.handleRobotSpawnedIntegrationEvent(event)
            is RobotUpgradedIntegrationEvent -> robotIntegrationEventHandler.handleRobotUpgradedIntegrationEvent(event)
            is RobotRestoredAttributesIntegrationEvent -> robotIntegrationEventHandler.handleRobotRestoredAttributesIntegrationEvent(event)
            is RobotResourceRemovedIntegrationEvent -> robotIntegrationEventHandler.handleRobotResourceRemovedIntegrationEvent(event)
            is RobotResourceMinedIntegrationEvent -> robotIntegrationEventHandler.handleRobotResourceMinedIntegrationEvent(event)
            is RobotRegeneratedIntegrationEvent -> robotIntegrationEventHandler.handleRobotRegeneratedIntegrationEvent(event)
            is RobotMovedIntegrationEvent -> robotIntegrationEventHandler.handleRobotMovedIntegrationEvent(event)
            is RobotAttackedIntegrationEvent -> robotIntegrationEventHandler.handleRobotAttackedIntegrationEvent(event)
            is RobotsRevealedIntegrationEvent -> {} // No use for this information. Discard it.
            else -> println(event)
        }
    }

}