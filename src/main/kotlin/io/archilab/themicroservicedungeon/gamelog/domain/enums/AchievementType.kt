package io.archilab.themicroservicedungeon.gamelog.domain.enums

enum class AchievementType(val id: Int) {
    FIGHTING_BRONZE(1),
    FIGHTING_SILVER(2),
    FIGHTING_GOLD(3),
    MINING_BRONZE(4),
    MINING_SILVER(5),
    MINING_GOLD(6),
    TRADING_BRONZE(7),
    TRADING_SILVER(8),
    TRADING_GOLD(9),
    TRAVELING_BRONZE(10),
    TRAVELING_SILVER(11),
    TRAVELING_GOLD(12)
}