package io.archilab.themicroservicedungeon.gamelog.domain.enums

enum class BankAccountTransactionType {
    INITIALIZED,
    CLEARED,
    TRADE
}