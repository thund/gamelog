package io.archilab.themicroservicedungeon.gamelog.domain.entities

import java.util.*
import javax.persistence.Column
import javax.persistence.Embeddable
import javax.persistence.ManyToOne

@Embeddable
class PlayerAchievement() {

    @Column
    lateinit var gameId: UUID

    @ManyToOne
    lateinit var achievement: Achievement

    constructor(gameId: UUID, achievement: Achievement) : this() {
        this.gameId = gameId
        this.achievement = achievement
    }

    fun matches(other: PlayerAchievement?): Boolean {
        if (other == null) return false
        return achievement == other.achievement
    }

}