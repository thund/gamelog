package io.archilab.themicroservicedungeon.gamelog.domain.entities

import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.UpgradeType
import org.hibernate.annotations.Type
import org.slf4j.LoggerFactory
import java.util.*
import javax.persistence.*

@Entity
class Robot () {

    companion object {

        private val logger = LoggerFactory.getLogger(Robot::class.java)

    }

    @Id
    @Column
    @Type(type = "uuid-char")
    lateinit var robotId: UUID

    @Column
    @Type(type = "uuid-char")
    var gameId: UUID? = null

    @Column
    var alive: Boolean = true

    @Column
    lateinit var playerId: UUID

    @Column
    var maxHealth: Int = 0

    @Column
    var maxEnergy: Int = 0

    @Column
    var energyRegeneration: Int = 0

    @Column
    var attackDamage: Int = 0

    @Column
    var miningSpeed: Int = 0

    @Column
    var health: Int = 0

    @Column
    var energy: Int = 0

    @Column
    var healthLevel: Int = 0

    @Column
    var damageLevel: Int = 0

    @Column
    var miningSpeedLevel: Int = 0

    @Column
    var miningLevel: Int = 0

    @Column
    var energyLevel: Int = 0

    @Column
    var energyRegenerationLevel: Int = 0

    @Column
    var storageLevel: Int = 0

    @Column
    var destroyedBy: UUID? = null

    @Embedded
    var inventory: Inventory = Inventory()

    @OneToMany(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH])
    @OrderBy("id")
    lateinit var movements: MutableList<Movement>

    @Embedded
    lateinit var minedResources: MinedResources

    @OneToMany(cascade = [CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH])
    lateinit var fights: Set<Fight>

    @Column
    @Type(type = "uuid-char")
    private var spawnPlanetId: UUID? = null

    @Column
    @Type(type = "uuid-char")
    var currentPlanetId: UUID? = null

    val previousPlanetId: UUID?
        get() {
            return movements.let {
                if (it.isEmpty()) spawnPlanetId else it.last().originPlanetId
            }
        }

    val offensiveFights: List<Fight>
        get() {
            return fights.filter {
                it.attackingRobotId == this.robotId
            }
        }

    val defensiveFights: List<Fight>
        get() {
            return fights.filter {
                it.defendingRobotId == this.robotId
            }
        }

    val kills: List<UUID>
        get() {
            return fights.filter {
                it.attackingRobotId == this.robotId && it.defendingRobotDestroyed
            }.map { it.defendingRobotId }
        }

    fun spawn(spawnPlanetId: UUID) {
        this.spawnPlanetId = spawnPlanetId
        currentPlanetId = spawnPlanetId
    }

    fun addFight(fight: Fight) {
        fights = fights.plus(fight)
    }

    fun addFight(attackerId: UUID, defenderId: UUID, energy: Int, health: Int, alive: Boolean, defenderAlive: Boolean) {
        addFight(Fight(attackerId, defenderId, !defenderAlive))
        this.energy = energy
        this.health = health
        this.alive = alive
        if (!alive) {
            destroyedBy = attackerId
        }
    }

    fun addMovement(originPlanetId: UUID, destinationPlanetId: UUID) {
        movements.add(Movement(robotId, originPlanetId, destinationPlanetId))
        currentPlanetId = destinationPlanetId
    }

    fun addMovement(originPlanetId: UUID, destinationPlanetId: UUID, energyAfterMovement: Int) {
        addMovement(originPlanetId, destinationPlanetId)
        energy = energyAfterMovement
    }

    fun installUpgrade(level: Int, upgrade: UpgradeType) {
        val currentLevel = getUpgradeLevel(upgrade)
        if (currentLevel > level) {
            logger.warn("New level ($level) for $upgrade is lower than the current level ($currentLevel). Level will be downgraded!")
        }
        setUpgradeLevel(upgrade, level)
    }

    fun processMinedResources(resource: ResourceType, amount: Int, inventoryAfterMining: Inventory) {
        minedResources.apply {
            when (resource) {
                ResourceType.COAL -> coal += amount
                ResourceType.IRON -> iron += amount
                ResourceType.GEM -> gem += amount
                ResourceType.GOLD -> gold += amount
                ResourceType.PLATIN -> platin += amount
            }
        }
        inventory = inventoryAfterMining
    }

    private fun getUpgradeLevel(upgrade: UpgradeType): Int {
        return when (upgrade) {
            UpgradeType.STORAGE -> this.storageLevel
            UpgradeType.HEALTH -> this.healthLevel
            UpgradeType.DAMAGE -> this.damageLevel
            UpgradeType.MINING_SPEED -> this.miningSpeedLevel
            UpgradeType.MINING -> this.miningLevel
            UpgradeType.MAXIMUM_ENERGY -> this.energyLevel
            UpgradeType.ENERGY_REGENERATION -> this.energyRegenerationLevel
        }
    }

    private fun setUpgradeLevel(upgrade: UpgradeType, level: Int) {
        when (upgrade) {
            UpgradeType.STORAGE -> this.storageLevel = level
            UpgradeType.HEALTH -> this.healthLevel = level
            UpgradeType.DAMAGE -> this.damageLevel = level
            UpgradeType.MINING_SPEED -> this.miningSpeedLevel = level
            UpgradeType.MINING -> this.miningLevel = level
            UpgradeType.MAXIMUM_ENERGY -> this.energyLevel = level
            UpgradeType.ENERGY_REGENERATION -> this.energyRegenerationLevel = level
        }
    }

    constructor(
        robotId: UUID,
        alive: Boolean,
        playerId: UUID,
        maxHealth: Int,
        maxEnergy: Int,
        energyRegeneration: Int,
        attackDamage: Int,
        miningSpeed: Int,
        health: Int,
        energy: Int,
        healthLevel: Int,
        damageLevel: Int,
        miningSpeedLevel: Int,
        miningLevel: Int,
        energyLevel: Int,
        energyRegenerationLevel: Int,
        storageLevel: Int
    ) : this() {
        this.robotId = robotId
        this.alive = alive
        this.playerId = playerId
        this.maxHealth = maxHealth
        this.maxEnergy = maxEnergy
        this.energyRegeneration = energyRegeneration
        this.attackDamage = attackDamage
        this.miningSpeed = miningSpeed
        this.health = health
        this.energy = energy
        this.healthLevel = healthLevel
        this.damageLevel = damageLevel
        this.miningSpeedLevel = miningSpeedLevel
        this.miningLevel = miningLevel
        this.energyLevel = energyLevel
        this.energyRegenerationLevel = energyRegenerationLevel
        this.storageLevel = storageLevel
        this.inventory = Inventory()
        this.movements = mutableListOf()
        this.minedResources = MinedResources()
        this.fights = setOf()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Robot

        if (robotId != other.robotId) return false
        if (alive != other.alive) return false
        if (playerId != other.playerId) return false
        if (maxHealth != other.maxHealth) return false
        if (maxEnergy != other.maxEnergy) return false
        if (energyRegeneration != other.energyRegeneration) return false
        if (attackDamage != other.attackDamage) return false
        if (miningSpeed != other.miningSpeed) return false
        if (health != other.health) return false
        if (energy != other.energy) return false
        if (healthLevel != other.healthLevel) return false
        if (damageLevel != other.damageLevel) return false
        if (miningSpeedLevel != other.miningSpeedLevel) return false
        if (miningLevel != other.miningLevel) return false
        if (energyLevel != other.energyLevel) return false
        if (energyRegenerationLevel != other.energyRegenerationLevel) return false
        if (storageLevel != other.storageLevel) return false
        if (inventory != other.inventory) return false
        if (movements != other.movements) return false
        if (minedResources != other.minedResources) return false
        if (fights != other.fights) return false

        return true
    }

    override fun hashCode(): Int {
        var result = robotId.hashCode()
        result = 31 * result + alive.hashCode()
        result = 31 * result + playerId.hashCode()
        result = 31 * result + maxHealth
        result = 31 * result + maxEnergy
        result = 31 * result + energyRegeneration
        result = 31 * result + attackDamage
        result = 31 * result + miningSpeed
        result = 31 * result + health
        result = 31 * result + energy
        result = 31 * result + healthLevel
        result = 31 * result + damageLevel
        result = 31 * result + miningSpeedLevel
        result = 31 * result + miningLevel
        result = 31 * result + energyLevel
        result = 31 * result + energyRegenerationLevel
        result = 31 * result + storageLevel
        result = 31 * result + inventory.hashCode()
        result = 31 * result + movements.hashCode()
        result = 31 * result + minedResources.hashCode()
        result = 31 * result + fights.hashCode()
        return result
    }


}