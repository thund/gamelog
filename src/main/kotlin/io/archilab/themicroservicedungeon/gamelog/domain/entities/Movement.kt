package io.archilab.themicroservicedungeon.gamelog.domain.entities

import org.hibernate.annotations.Type
import java.math.BigInteger
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Movement() {

    @Id
    @GeneratedValue
    lateinit var id: BigInteger

    @Column
    @Type(type = "uuid-char")
    lateinit var robotId: UUID

    @Column
    @Type(type = "uuid-char")
    lateinit var originPlanetId: UUID

    @Column
    @Type(type = "uuid-char")
    lateinit var destinationPlanetId: UUID

    constructor(robotId: UUID, originPlanetId: UUID, destinationPlanetId: UUID) : this() {
        this.robotId = robotId
        this.originPlanetId = originPlanetId
        this.destinationPlanetId = destinationPlanetId
    }

}