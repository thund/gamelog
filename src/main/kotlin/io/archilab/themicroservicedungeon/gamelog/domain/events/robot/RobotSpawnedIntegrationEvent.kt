import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.RobotProperties

data class RobotSpawnedIntegrationEvent(
    val robot: RobotProperties
) : InternalEvent