package io.archilab.themicroservicedungeon.gamelog.domain.events.robot.legacy

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import java.util.*

data class RobotAttackedEvent(
    @JsonProperty("attacker")
    val attackingRobotId: UUID,
    @JsonProperty("defender")
    val defendingRobotId: UUID
) : InternalEvent