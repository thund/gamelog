package io.archilab.themicroservicedungeon.gamelog.domain.events.trading

import com.fasterxml.jackson.annotation.JacksonInject
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import java.time.Instant
import java.util.UUID

data class BankAccountTransactionBookedEvent(
    val playerId: UUID,
    val transactionAmount: Int,
    val balance: Int,
    @JacksonInject("timestamp")
    val timestamp: Instant
) : InternalEvent