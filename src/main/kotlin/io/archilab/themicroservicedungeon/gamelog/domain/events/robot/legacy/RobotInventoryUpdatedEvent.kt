package io.archilab.themicroservicedungeon.gamelog.domain.events.robot.legacy

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.Inventory
import java.util.*

data class RobotInventoryUpdatedEvent(
    @JsonProperty("robot")
    val robotId: UUID,
    val inventory: Inventory
) : InternalEvent