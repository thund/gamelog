package io.archilab.themicroservicedungeon.gamelog.domain.trophies

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Trophy
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ScoreboardCategory
import io.archilab.themicroservicedungeon.gamelog.domain.enums.TrophyType
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.TrophyRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class TrophiesManager {

    @Autowired
    private lateinit var trophyRepository: TrophyRepository

    private val scoreboardTrophies = listOf(
        Trophy(
            TrophyType.GameFirstPlace.id, "Game First Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Game%20Score%20-%20First%20Place.png"),
        Trophy(
            TrophyType.GameSecondPlace.id, "Game Second Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Game%20Score%20-%20Second%20Place.png"),
        Trophy(
            TrophyType.GameThirdPlace.id, "Game Third Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Game%20Score%20-%20Third%20Place.png"),
        Trophy(
            TrophyType.FightingFirstPlace.id, "Fighting First Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Fighting%20Score%20-%20First%20Place.png"),
        Trophy(
            TrophyType.FightingSecondPlace.id, "Fighting Second Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Fighting%20Score%20-%20Second%20Place.png"),
        Trophy(
            TrophyType.FightingThirdPlace.id, "Fighting Third Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Fighting%20Score%20-%20Third%20Place.png"),
        Trophy(
            TrophyType.MiningFirstPlace.id, "Mining First Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Mining%20Score%20-%20First%20Place.png"),
        Trophy(
            TrophyType.MiningSecondPlace.id, "Mining Second Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Mining%20Score%20-%20Second%20Place.png"),
        Trophy(
            TrophyType.MiningThirdPlace.id, "Mining Third Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Mining%20Score%20-%20Third%20Place.png"),
        Trophy(
            TrophyType.TradingFirstPlace.id, "Trading First Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Trading%20Score%20-%20First%20Place.png"),
        Trophy(
            TrophyType.TradingSecondPlace.id, "Trading Second Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Trading%20Score%20-%20Second%20Place.png"),
        Trophy(
            TrophyType.TradingThirdPlace.id, "Trading Third Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Trading%20Score%20-%20Third%20Place.png"),
        Trophy(
            TrophyType.TravelingFirstPlace.id, "Traveling First Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Traveling%20Score%20-%20First%20Place.png"),
        Trophy(
            TrophyType.TravelingSecondPlace.id, "Traveling Second Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Traveling%20Score%20-%20Second%20Place.png"),
        Trophy(
            TrophyType.TravelingThirdPlace.id, "Traveling Third Place",
            "https://gitlab.com/the-microservice-dungeon/core-services/gamelog/-/raw/main/assets/scoreboard/Traveling%20Score%20-%20Third%20Place.png")
    )

    fun getTrophy(type: TrophyType): Trophy? {
        return trophyRepository.retrieve(type) ?: scoreboardTrophies.find { trophy -> trophy.id == type.id }?.also {
            trophyRepository.store(it)
        }
    }

    fun getScoreboardCategory(type: TrophyType) : ScoreboardCategory {
        return when(type) {
            TrophyType.GameFirstPlace, TrophyType.GameSecondPlace, TrophyType.GameThirdPlace -> ScoreboardCategory.GAME
            TrophyType.FightingFirstPlace, TrophyType.FightingSecondPlace, TrophyType.FightingThirdPlace -> ScoreboardCategory.FIGHTING
            TrophyType.MiningFirstPlace, TrophyType.MiningSecondPlace, TrophyType.MiningThirdPlace -> ScoreboardCategory.MINING
            TrophyType.TradingFirstPlace, TrophyType.TradingSecondPlace, TrophyType.TradingThirdPlace -> ScoreboardCategory.TRADING
            TrophyType.TravelingFirstPlace, TrophyType.TravelingSecondPlace, TrophyType.TravelingThirdPlace -> ScoreboardCategory.TRAVELING
        }
    }

    fun getScoreboardCategory(id: Int) : ScoreboardCategory? {
        val trophyType = getTrophyType(id)
        return trophyType?.let { getScoreboardCategory(trophyType) }
    }

    private fun getTrophyType(id: Int): TrophyType? {
        return TrophyType.values().find { trophyType -> trophyType.id == id }
    }

}