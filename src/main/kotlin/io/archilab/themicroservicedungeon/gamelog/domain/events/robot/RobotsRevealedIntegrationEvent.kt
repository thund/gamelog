package io.archilab.themicroservicedungeon.gamelog.domain.events.robot

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.RevealedRobotProperties
import java.util.*

data class RobotsRevealedIntegrationEvent (
    @JsonProperty("robots")
    val robots: List<RevealedRobotProperties>
) : InternalEvent