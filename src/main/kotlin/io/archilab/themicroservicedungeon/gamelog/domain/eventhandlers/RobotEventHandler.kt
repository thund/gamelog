package io.archilab.themicroservicedungeon.gamelog.domain.eventhandlers

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Fight
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Inventory
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import io.archilab.themicroservicedungeon.gamelog.domain.enums.UpgradeType
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.legacy.*
import io.archilab.themicroservicedungeon.gamelog.domain.mappers.SpawnRobotPropertiesToRobotMapper
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlayerRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.RobotRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
class RobotEventHandler {

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var robotRepository: RobotRepository

    private val robotPropertiesToRobotMapper = SpawnRobotPropertiesToRobotMapper()

    @Transactional
    fun handleRobotSpawnedEvent(event: RobotSpawnedEvent) {
        val player = playerRepository.getPlayerById(event.robot.playerId)
        player?.apply {
            val robot = robotPropertiesToRobotMapper.mapRobotPropertiesToRobot(event.robot)
            addRobot(robot)
            playerRepository.savePlayer(this)
        }
    }

    @Transactional
    fun handleRobotKilledEvent(event: RobotKilledEvent) {
        val robot = robotRepository.getRobotById(event.killedRobotId)
        robot?.let {
            it.alive = false
            robotRepository.save(it)
        }
    }

    @Transactional
    fun handleRobotEnergyUpdatedEvent(event: RobotEnergyUpdatedEvent) {
        val robot = robotRepository.getRobotById(event.robotId)
        robot?.apply {
            energy = event.energy
            robotRepository.save(this)
        }
    }

    @Transactional
    fun handleRobotHealthUpdatedEvent(event: RobotHealthUpdatedEvent) {
        val robot = robotRepository.getRobotById(event.robotId)
        robot?.apply {
            health = event.health
            robotRepository.save(this)
        }
    }

    @Transactional
    fun handleRobotInventoryUpdatedEvent(event: RobotInventoryUpdatedEvent) {
        val robot = robotRepository.getRobotById(event.robotId)
        robot?.apply {
            inventory = event.inventory.resources.let {
                Inventory(it.coal, it.iron, it.gem, it.gold, it.platin)
            }
            robotRepository.save(this)
        }
    }

    @Transactional
    fun handleRobotMovedEvent(event: RobotMovedEvent) {
        val robot = robotRepository.getRobotById(event.robotId)
        robot?.apply {
            addMovement(event.originPlanetId, event.destinationPlanetId)
            robotRepository.save(this)
        }
    }

    @Transactional
    fun handleRobotResourceMinedEvent(event: RobotResourceMinedEvent) {
        val robot = robotRepository.getRobotById(event.robotId)
        robot?.apply {
            when (event.resource) {
                ResourceType.COAL -> minedResources.coal += event.amount
                ResourceType.IRON -> minedResources.iron += event.amount
                ResourceType.GEM -> minedResources.gem += event.amount
                ResourceType.GOLD -> minedResources.gold += event.amount
                ResourceType.PLATIN -> minedResources.platin += event.amount
            }
            robotRepository.save(this)
        }
    }

    @Transactional
    fun handleRobotAttackedEvent(event: RobotAttackedEvent) {
        val attackingRobot = robotRepository.getRobotById(event.attackingRobotId)
        val defendingRobot = robotRepository.getRobotById(event.defendingRobotId)
        if (attackingRobot != null && defendingRobot != null) {
            val fight = Fight(event.attackingRobotId, event.defendingRobotId, false)
            attackingRobot.addFight(fight)
            defendingRobot.addFight(fight)
            robotRepository.save(attackingRobot)
            robotRepository.save(defendingRobot)
        }
    }

    @Transactional
    fun handleRobotUpgradedEvent(event: RobotUpgradedEvent) {
        val robot = robotRepository.getRobotById(event.robotId)
        robot?.apply {
            when(event.upgrade) {
                UpgradeType.STORAGE -> storageLevel = event.level
                UpgradeType.HEALTH -> healthLevel = event.level
                UpgradeType.DAMAGE -> damageLevel = event.level
                UpgradeType.MINING_SPEED -> miningSpeedLevel = event.level
                UpgradeType.MINING -> miningLevel = event.level
                UpgradeType.MAXIMUM_ENERGY -> energyLevel = event.level
                UpgradeType.ENERGY_REGENERATION -> energyRegenerationLevel = event.level
            }
            robotRepository.save(this)
        }
    }

}