package io.archilab.themicroservicedungeon.gamelog.domain.entities

import java.math.BigInteger
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Fight() {

    @Id
    @GeneratedValue
    lateinit var id: BigInteger

    @Column
    lateinit var attackingRobotId: UUID

    @Column
    lateinit var defendingRobotId: UUID

    @Column
    var defendingRobotDestroyed: Boolean = false

    constructor(attackingRobotId: UUID, defendingRobotId: UUID, defendingRobotDestroyed: Boolean) :this() {
        this.attackingRobotId = attackingRobotId
        this.defendingRobotId = defendingRobotId
        this.defendingRobotDestroyed = defendingRobotDestroyed
    }

}