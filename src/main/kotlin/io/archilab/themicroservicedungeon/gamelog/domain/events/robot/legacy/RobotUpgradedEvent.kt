package io.archilab.themicroservicedungeon.gamelog.domain.events.robot.legacy

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.enums.UpgradeType
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import java.util.*

data class RobotUpgradedEvent (
    @JsonProperty("robot")
    val robotId: UUID,
    @JsonProperty("level")
    val level: Int,
    @JsonProperty("type")
    val upgrade: UpgradeType
) : InternalEvent