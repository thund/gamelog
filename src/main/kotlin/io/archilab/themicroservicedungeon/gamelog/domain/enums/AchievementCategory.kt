package io.archilab.themicroservicedungeon.gamelog.domain.enums

enum class AchievementCategory {
    FIGHTING,
    MINING,
    TRADING,
    TRAVELING
}