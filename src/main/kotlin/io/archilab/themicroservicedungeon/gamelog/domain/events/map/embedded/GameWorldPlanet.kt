package io.archilab.themicroservicedungeon.gamelog.domain.events.map.embedded

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class GameWorldPlanet(
    @JsonProperty("id")
    val planetId: UUID,
    val x: Int,
    val y: Int,
    val movementDifficulty: Int,
    val resource: GameWorldResource?
)