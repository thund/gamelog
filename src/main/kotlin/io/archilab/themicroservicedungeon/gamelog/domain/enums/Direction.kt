package io.archilab.themicroservicedungeon.gamelog.domain.enums

import com.fasterxml.jackson.annotation.JsonValue

enum class Direction (@get:JsonValue val direction: String) {
    NORTH("NORTH"),
    SOUTH("SOUTH"),
    EAST("EAST"),
    WEST("WEST"),
}