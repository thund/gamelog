package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import io.archilab.themicroservicedungeon.gamelog.domain.enums.AchievementCategory

data class AchievementDto(
    val name: String,
    val image: String,
    val category: AchievementCategory
)