package io.archilab.themicroservicedungeon.gamelog.domain.events.robot.legacy

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import java.util.*

data class RobotResourceMinedEvent(
    @JsonProperty("robot")
    val robotId: UUID,
    val amount: Int,
    val resource: ResourceType
) : InternalEvent
