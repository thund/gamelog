package io.archilab.themicroservicedungeon.gamelog.domain.mappers

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Inventory
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded.ResourceInventory

class ResourceInventoryToInventoryMapper {

    fun mapResourceInventoryToInventory(resourceInventory: ResourceInventory) : Inventory {
        return with(resourceInventory) {
            Inventory(coal, iron, gem, gold, platin)
        }
    }

}