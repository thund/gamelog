package io.archilab.themicroservicedungeon.gamelog.domain.events.robot.legacy

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import java.util.*

data class RobotMovedEvent (
    @JsonProperty("robot")
    val robotId: UUID,
    @JsonProperty("fromPlanet")
    val originPlanetId: UUID,
    @JsonProperty("toPlanet")
    val destinationPlanetId: UUID
        ) : InternalEvent