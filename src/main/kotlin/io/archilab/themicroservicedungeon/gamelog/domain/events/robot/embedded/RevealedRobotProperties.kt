package io.archilab.themicroservicedungeon.gamelog.domain.events.robot.embedded

import com.fasterxml.jackson.annotation.JsonProperty
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.RobotLevels
import java.util.*

data class RevealedRobotProperties (
    @JsonProperty("robotId")
    val robotId: UUID,
    @JsonProperty("planetId")
    val planetId: UUID,
    @JsonProperty("playerNotion")
    val playerNotion: String,
    @JsonProperty("health")
    val health: Int,
    @JsonProperty("energy")
    val energy: Int,
    @JsonProperty("levels")
    val levels: RobotLevels
)