package io.archilab.themicroservicedungeon.gamelog.domain.events.trading.embedded

import io.archilab.themicroservicedungeon.gamelog.domain.enums.TradableType
import java.math.BigDecimal

data class TradablePrice(
    val name: String,
    val price: BigDecimal,
    val type: TradableType
)