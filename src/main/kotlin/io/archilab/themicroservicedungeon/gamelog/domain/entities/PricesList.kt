package io.archilab.themicroservicedungeon.gamelog.domain.entities

import java.time.Instant
import javax.persistence.Column
import javax.persistence.ElementCollection
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class PricesList() {

    @Id
    @GeneratedValue
    var id: Long = -1

    @Column
    var timestamp: Instant = Instant.now()

    @ElementCollection
    var prices: MutableList<Price> = mutableListOf()

}