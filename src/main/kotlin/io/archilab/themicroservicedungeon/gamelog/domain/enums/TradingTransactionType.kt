package io.archilab.themicroservicedungeon.gamelog.domain.enums

enum class TradingTransactionType {
    BOUGHT,
    SOLD
}