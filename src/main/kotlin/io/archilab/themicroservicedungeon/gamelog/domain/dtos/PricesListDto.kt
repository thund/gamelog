package io.archilab.themicroservicedungeon.gamelog.domain.dtos

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Price
import io.archilab.themicroservicedungeon.gamelog.domain.entities.PricesList
import java.time.Instant

data class PricesListDto(
    val timestamp: Instant,
    val prices: List<Price>
) {
    constructor(pricesList: PricesList) : this(pricesList.timestamp, pricesList.prices)
}