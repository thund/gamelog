package io.archilab.themicroservicedungeon.gamelog.adapters.database.repositories

import io.archilab.themicroservicedungeon.gamelog.adapters.database.interfaces.RobotCrudRepository
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Robot
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.RobotRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class RobotJpaRepository : RobotRepository {

    @Autowired
    private lateinit var robotCrudRepository: RobotCrudRepository

    override fun getRobotById(robotId: UUID): Robot? {
        return robotCrudRepository.findByIdOrNull(robotId)
    }

    override fun getRobotsForPlanet(planetId: UUID): List<Robot> {
        return robotCrudRepository.findRobotsByCurrentPlanetId(planetId)
    }

    override fun save(robot: Robot) {
        robotCrudRepository.save(robot)
    }
}