package io.archilab.themicroservicedungeon.gamelog.adapters.database.interfaces

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Planet
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface PlanetCrudRepository : CrudRepository<Planet, UUID> {
}