package io.archilab.themicroservicedungeon.gamelog.adapters.database.repositories

import io.archilab.themicroservicedungeon.gamelog.adapters.database.interfaces.PlanetCrudRepository
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Planet
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlanetRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class PlanetJpaRepository : PlanetRepository {

    @Autowired
    private lateinit var planetCrudRepository: PlanetCrudRepository

    override fun getPlanetById(planetId: UUID): Planet? {
        return planetCrudRepository.findByIdOrNull(planetId)
    }

    override fun save(planet: Planet) {
        planetCrudRepository.save(planet)
    }

}