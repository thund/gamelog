package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.listeners

import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers.BankEventMapper
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers.PricesEventMapper
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers.TradeBuyEventMapper
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers.TradeSellEventMapper
import io.archilab.themicroservicedungeon.gamelog.domain.broker.InternalEventPublisher
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Header
import org.springframework.stereotype.Component

@Component
@Profile("!api-test & !no-kafka")
class TradingServiceEventListener {

    @Autowired
    private lateinit var bankEventMapper: BankEventMapper

    @Autowired
    private lateinit var pricesEventMapper: PricesEventMapper

    @Autowired
    private lateinit var tradeSellEventMapper: TradeSellEventMapper

    @Autowired
    private lateinit var tradeBuyEventMapper: TradeBuyEventMapper

    @Autowired
    private lateinit var internalEventPublisher: InternalEventPublisher

    @KafkaListener(topics = ["bank"])
    fun bankEvent(@Header("type") type: String, @Header("timestamp") timestamp: String, payload: ConsumerRecord<String, ByteArray>) {
        val event = bankEventMapper.mapConsumerRecordToEvent(type, timestamp, payload)
        internalEventPublisher.publishEvent(event)
    }

    @KafkaListener(topics = ["prices"])
    fun pricesEvent(@Header("type") type: String, @Header("timestamp") timestamp: String, payload: ConsumerRecord<String, ByteArray>) {
        val event = pricesEventMapper.mapConsumerRecordToEvent(type, timestamp, payload)
        internalEventPublisher.publishEvent(event)
    }

    @KafkaListener(topics = ["trade-sell"])
    fun tradeSellEvent(@Header("type") type: String, @Header("timestamp") timestamp: String, payload: ConsumerRecord<String, ByteArray>) {
        val event = tradeSellEventMapper.mapConsumerRecordToEvent(type, timestamp, payload)
        internalEventPublisher.publishEvent(event)
    }

    @KafkaListener(topics = ["trade-buy"])
    fun tradeBuyEvent(@Header("type") type: String, @Header("timestamp") timestamp: String, payload: ConsumerRecord<String, ByteArray>) {
        val event = tradeBuyEventMapper.mapConsumerRecordToEvent(type, timestamp, payload)
        internalEventPublisher.publishEvent(event)
    }

}