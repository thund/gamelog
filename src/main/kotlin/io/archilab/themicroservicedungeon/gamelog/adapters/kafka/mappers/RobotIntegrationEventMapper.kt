package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import RobotSpawnedIntegrationEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.*
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class RobotIntegrationEventMapper : KafkaToInternalEventMapper {

    private val objectMapper = InjectingObjectMapper()

    override fun mapConsumerRecordToEvent(type: String, timestamp: String, payload: ConsumerRecord<String, ByteArray>): InternalEvent {
        val eventClass = when (type) {
            "RobotAttackedIntegrationEvent" -> RobotAttackedIntegrationEvent::class.java
            "RobotMovedIntegrationEvent" -> RobotMovedIntegrationEvent::class.java
            "RobotRegeneratedIntegrationEvent" -> RobotRegeneratedIntegrationEvent::class.java
            "RobotResourceMinedIntegrationEvent" -> RobotResourceMinedIntegrationEvent::class.java
            "RobotResourceRemovedIntegrationEvent" -> RobotResourceRemovedIntegrationEvent::class.java
            "RobotRestoredAttributesIntegrationEvent" -> RobotRestoredAttributesIntegrationEvent::class.java
            "RobotSpawnedIntegrationEvent" -> RobotSpawnedIntegrationEvent::class.java
            "RobotUpgradedIntegrationEvent" -> RobotUpgradedIntegrationEvent::class.java
            "RobotsRevealedIntegrationEvent" -> RobotsRevealedIntegrationEvent::class.java
            else -> throw MapperUnknownEventTypeException()
        }
        return objectMapper.mapInjectingValues(mapOf(Pair("timestamp", Instant.parse(timestamp))), payload.value(), eventClass)
    }

}