package io.archilab.themicroservicedungeon.gamelog.adapters.database.mocks

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.*
import io.archilab.themicroservicedungeon.gamelog.domain.enums.TrophyType
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.ScoreboardTrophiesRepository
import io.archilab.themicroservicedungeon.gamelog.domain.trophies.TrophiesManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import java.util.*

@Repository
@Profile("api-test")
class ScoreboardTrophiesRepositoryMock : ScoreboardTrophiesRepository {

    @Autowired
    private lateinit var trophiesManager: TrophiesManager

    private val playerNames = listOf(
        "foo",
        "bar",
        "baz",
        "lorem",
        "ipsum"
    )

    private val gameId = UUID.randomUUID()

    override fun getScoreboardTrophies(): ScoreboardTrophiesDto {
        val scoreboardTrophies = generateRandomPlayerScoreboardTrophies()
        return ScoreboardTrophiesDto(
            gameId,
            scoreboardTrophies
        )
    }

    private fun generateRandomPlayerScoreboardTrophies() : List<PlayerScoreboardTrophiesDto> {
        return playerNames.mapIndexed { index, name ->
            val player = PlayerDto(UUID.randomUUID(), name)
            PlayerScoreboardTrophiesDto(
                player,
                generateRandomScoreboardTrophies(index),
                gameId
            )
        }
    }

    private fun generateRandomScoreboardTrophies(index: Int) : List<ScoreboardTrophyDto> {
        val trophyTypes = when (index) {
            0 -> listOf( TrophyType.GameFirstPlace, TrophyType.FightingFirstPlace, TrophyType.MiningFirstPlace, TrophyType.TradingFirstPlace, TrophyType.TravelingFirstPlace)
            1 -> listOf( TrophyType.GameSecondPlace, TrophyType.FightingSecondPlace, TrophyType.MiningSecondPlace, TrophyType.TradingSecondPlace, TrophyType.TravelingSecondPlace)
            2 -> listOf( TrophyType.GameThirdPlace, TrophyType.FightingThirdPlace, TrophyType.MiningThirdPlace, TrophyType.TradingThirdPlace, TrophyType.TravelingThirdPlace)
            else -> listOf()
        }
        return trophyTypes.mapNotNull {
            trophiesManager.getTrophy(it)?.let { trophy ->
                ScoreboardTrophyDto(
                    trophy.name, trophy.imageUrl, trophiesManager.getScoreboardCategory(it)
                )
            }
        }
    }

}