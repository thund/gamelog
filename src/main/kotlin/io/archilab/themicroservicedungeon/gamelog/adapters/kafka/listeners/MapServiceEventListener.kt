package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.listeners

import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers.GameWorldEventMapper
import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers.PlanetEventMapper
import io.archilab.themicroservicedungeon.gamelog.domain.broker.InternalEventPublisher
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Header
import org.springframework.stereotype.Component

@Component
@Profile("!api-test & !no-kafka")
class MapServiceEventListener {

    @Autowired
    private lateinit var gameWorldEventMapper: GameWorldEventMapper

    @Autowired
    private lateinit var planetEventMapper: PlanetEventMapper

    @Autowired
    private lateinit var internalEventPublisher: InternalEventPublisher

    @KafkaListener(topics = ["gameworld"])
    fun gameWorldEvent(@Header("type") type: String, payload: ConsumerRecord<String, ByteArray>) {
        val event = gameWorldEventMapper.mapConsumerRecordToEvent(type, "", payload)
        internalEventPublisher.publishEvent(event)
    }

    @KafkaListener(topics = ["planet"])
    fun planetEvent(@Header("type") type: String, payload: ConsumerRecord<String, ByteArray>) {
        val event = planetEventMapper.mapConsumerRecordToEvent(type, "", payload)
        internalEventPublisher.publishEvent(event)
    }

}