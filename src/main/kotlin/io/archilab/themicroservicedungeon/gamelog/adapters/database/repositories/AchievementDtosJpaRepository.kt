package io.archilab.themicroservicedungeon.gamelog.adapters.database.repositories

import io.archilab.themicroservicedungeon.gamelog.domain.achievements.AchievementsManager
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.AchievementDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.AchievementsDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.PlayerAchievementDto
import io.archilab.themicroservicedungeon.gamelog.domain.dtos.PlayerDto
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.AchievementDtosRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.GameRepository
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlayerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Repository
@Profile("!api-test")
class AchievementDtosJpaRepository : AchievementDtosRepository {

    @Autowired
    private lateinit var playerRepository: PlayerRepository

    @Autowired
    private lateinit var gameRepository: GameRepository

    @Autowired
    private lateinit var achievementsManager: AchievementsManager

    override fun getAchievements(): AchievementsDto {
        return gameRepository.getNewestGame()?.let {game ->
            playerRepository.getPlayers().let {players ->
                AchievementsDto(
                    game.id,
                    players.flatMap {player ->
                        player.achievements.map {playerAchievement ->
                            PlayerAchievementDto(
                                PlayerDto(player.id, player.name),
                                playerAchievement.gameId,
                                AchievementDto(
                                    playerAchievement.achievement.name,
                                    playerAchievement.achievement.imageUrl,
                                    achievementsManager.getAchievementCategory(playerAchievement.achievement.id)!!
                                )
                            )
                        }
                    }
                )
            }
        }?: AchievementsDto(null, listOf())
    }

}