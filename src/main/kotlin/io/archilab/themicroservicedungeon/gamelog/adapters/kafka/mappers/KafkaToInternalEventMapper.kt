package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import org.apache.kafka.clients.consumer.ConsumerRecord

interface KafkaToInternalEventMapper {

    fun mapConsumerRecordToEvent(type: String, timestamp: String, payload: ConsumerRecord<String, ByteArray>): InternalEvent

}