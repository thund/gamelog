package io.archilab.themicroservicedungeon.gamelog.adapters.database.repositories

import io.archilab.themicroservicedungeon.gamelog.adapters.database.interfaces.PlayerCrudRepository
import io.archilab.themicroservicedungeon.gamelog.domain.entities.Player
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PlayerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class PlayerJpaRepository : PlayerRepository {

    @Autowired
    private lateinit var playerCrudRepository: PlayerCrudRepository

    override fun savePlayer(player: Player) {
        playerCrudRepository.save(player)
    }

    override fun getPlayerById(playerId: UUID): Player? {
        return playerCrudRepository.findByIdOrNull(playerId)
    }

    override fun getPlayers(): List<Player> {
        return playerCrudRepository.findAll().toList()
    }

}