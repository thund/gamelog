package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.trading.TradableSoldEvent
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.stereotype.Component

@Component
class TradeSellEventMapper : KafkaToInternalEventMapper {

    private val objectMapper = ObjectMapper().registerKotlinModule()

    override fun mapConsumerRecordToEvent(type: String, timestamp: String, payload: ConsumerRecord<String, ByteArray>): InternalEvent {
        if (type != "TradableSold") throw MapperUnknownEventTypeException()
        return objectMapper.readValue(payload.value(), TradableSoldEvent::class.java)
    }

}