package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.listeners

import io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers.RobotEventMapper
import io.archilab.themicroservicedungeon.gamelog.domain.broker.InternalEventPublisher
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.messaging.handler.annotation.Header
import org.springframework.stereotype.Component

@Component
@Profile("!api-test & !no-kafka & legacy-robot-topic")
class LegacyRobotServiceEventListener {

    @Autowired
    private lateinit var robotEventMapper: RobotEventMapper

    @Autowired
    private lateinit var internalEventPublisher: InternalEventPublisher

    @KafkaListener(topics = ["robot"])  // Legacy topic. Disabled to avoid conflicts with the new robot.integration topic.
    fun robotEvent(@Header("type") type: String, payload: ConsumerRecord<String, ByteArray>) {
        val event = robotEventMapper.mapConsumerRecordToEvent(type, "", payload)
        internalEventPublisher.publishEvent(event)
    }

}