package io.archilab.themicroservicedungeon.gamelog.adapters.kafka.mappers

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.archilab.themicroservicedungeon.gamelog.domain.events.InternalEvent
import io.archilab.themicroservicedungeon.gamelog.domain.events.robot.legacy.*
import io.archilab.themicroservicedungeon.gamelog.domain.exceptions.MapperUnknownEventTypeException
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.stereotype.Component

@Component
class RobotEventMapper : KafkaToInternalEventMapper {

    private val objectMapper = ObjectMapper().registerKotlinModule()

    override fun mapConsumerRecordToEvent(type: String, timestamp: String, payload: ConsumerRecord<String, ByteArray>): InternalEvent {
        val eventClass = when (type) {
            "RobotSpawned" -> RobotSpawnedEvent::class.java
            "RobotMoved" -> RobotMovedEvent::class.java
            "RobotAttacked" -> RobotAttackedEvent::class.java
            "RobotKilled" -> RobotKilledEvent::class.java
            "RobotHealthUpdated" -> RobotHealthUpdatedEvent::class.java
            "RobotEnergyUpdated" -> RobotEnergyUpdatedEvent::class.java
            "RobotResourceMined" -> RobotResourceMinedEvent::class.java
            "RobotInventoryUpdated" -> RobotInventoryUpdatedEvent::class.java
            "RobotUpgraded" -> RobotUpgradedEvent::class.java
            else -> throw MapperUnknownEventTypeException()
        }
        return objectMapper.readValue(payload.value(), eventClass)
    }

}