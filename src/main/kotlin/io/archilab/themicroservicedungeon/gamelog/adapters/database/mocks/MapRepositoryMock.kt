package io.archilab.themicroservicedungeon.gamelog.adapters.database.mocks

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.*
import io.archilab.themicroservicedungeon.gamelog.domain.enums.GameStatus
import io.archilab.themicroservicedungeon.gamelog.domain.enums.ResourceType
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.MapRepository
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import java.time.Instant
import java.util.*
import kotlin.random.Random
import kotlin.random.nextInt

@Repository
@Profile("api-test")
class MapRepositoryMock : MapRepository {

    private val gameId = UUID.randomUUID()
    private val roundNumber = Random.nextInt(1 .. 30)
    private val timestamp = Instant.now()
    private val mapSize = 2
    private val namesList = listOf("foo", "bar", "baz")

    private val map = generateMap()

    override fun getMap() : MapDto {
        return map
    }

    private fun generateMap(): MapDto {
        return MapDto(
            gameId,
            GameStatus.STARTED,
            roundNumber,
            timestamp,
            namesList,
            generatePlanetsList(mapSize)
        )
    }

    private fun generatePlanetsList(mapSize: Int) : List<PlanetDto> {
        val planets = mutableListOf<PlanetDto>()
        for (x in 0 until mapSize) {
            for (y in 0 until mapSize) {
                planets.add(
                    generatePlanet(x, y)
                )
            }
        }
        return planets
    }

    private fun generatePlanet(x: Int, y: Int) = PlanetDto(
        UUID.randomUUID(),
        x, y,
        Random.nextInt(1, 4),
        Random.nextInt(1, 4),
        generateResources(),
        generateRobots()
    )

    private fun generateResources(): List<ResourceDto> {
        val resourceType = randomResourceType() ?: return listOf()
        val maximumAmount = Random.nextInt(1, 11) * 10
        val currentAmount = Random.nextInt(0, maximumAmount)
        return listOf(ResourceDto(resourceType, maximumAmount, currentAmount))
    }

    private fun randomResourceType() : ResourceType? {
        return when (Random.nextInt(0, 6)) {
            0 -> ResourceType.COAL
            1 -> ResourceType.IRON
            2 -> ResourceType.GEM
            3 -> ResourceType.GOLD
            4 -> ResourceType.PLATIN
            else -> null
        }
    }

    private fun generateRobots(): List<RobotDto> {
        val numberOfRobots = Random.nextInt(0, 3)
        if (numberOfRobots == 0) return listOf()
        return (0 .. numberOfRobots).map {
            RobotDto(UUID.randomUUID())
        }.toList()
    }

}