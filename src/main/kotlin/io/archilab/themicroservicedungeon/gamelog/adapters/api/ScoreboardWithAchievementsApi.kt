package io.archilab.themicroservicedungeon.gamelog.adapters.api

import io.archilab.themicroservicedungeon.gamelog.domain.dtos.*
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.ScoreboardWithAchievementsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@CrossOrigin
@RequestMapping("scoreboard-with-achievements")
class ScoreboardWithAchievementsApi {

    @Autowired
    private lateinit var scoreboardWithAchievementsRepository: ScoreboardWithAchievementsRepository

    @GetMapping(produces = ["application/json"])
    fun getScoreboard(): ResponseEntity<ScoreboardWithAchievementsDto> {
        val scoreboardWithAchievements = scoreboardWithAchievementsRepository.getScoreboardWithAchievements()
        return ResponseEntity(scoreboardWithAchievements, HttpStatus.OK)
    }

}