package io.archilab.themicroservicedungeon.gamelog.adapters.database.mocks

import io.archilab.themicroservicedungeon.gamelog.domain.entities.Price
import io.archilab.themicroservicedungeon.gamelog.domain.entities.PricesList
import io.archilab.themicroservicedungeon.gamelog.domain.enums.TradableType
import io.archilab.themicroservicedungeon.gamelog.domain.repositories.PriceRepository
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import java.math.BigDecimal
import java.time.Instant

@Repository
@Profile("api-test")
class PriceRepositoryMock : PriceRepository {
    
    private var pricesList = generatePrices()
    
    override fun getNewestPrices(): PricesList? {
        return pricesList
    }

    override fun savePricesList(pricesList: PricesList) {
        this.pricesList = pricesList
    }
    
    final fun generatePrices(): PricesList {
        return PricesList().apply {
            timestamp = Instant.now()
            prices.apply {
                add(Price("MINING_SPEED_1", BigDecimal(50), TradableType.UPGRADE))
                add(Price("GOLD", BigDecimal(50), TradableType.RESOURCE))
                add(Price("MINING_SPEED_2", BigDecimal(300), TradableType.UPGRADE))
                add(Price("HEALTH_RESTORE", BigDecimal(50), TradableType.RESTORATION))
                add(Price("MINING_SPEED_5", BigDecimal(15000), TradableType.UPGRADE))
                add(Price("PLATIN", BigDecimal(60), TradableType.RESOURCE))
                add(Price("MINING_SPEED_3", BigDecimal(1500), TradableType.UPGRADE))
                add(Price("MAX_ENERGY_4", BigDecimal(4000), TradableType.UPGRADE))
                add(Price("MINING_SPEED_4", BigDecimal(4000), TradableType.UPGRADE))
                add(Price("MAX_ENERGY_5", BigDecimal(15000), TradableType.UPGRADE))
                add(Price("COAL", BigDecimal(5), TradableType.RESOURCE))
                add(Price("MAX_ENERGY_2", BigDecimal(300), TradableType.UPGRADE))
                add(Price("MAX_ENERGY_3", BigDecimal(1500), TradableType.UPGRADE))
                add(Price("ENERGY_REGEN_5", BigDecimal(15000), TradableType.UPGRADE))
                add(Price("MAX_ENERGY_1", BigDecimal(50), TradableType.UPGRADE))
                add(Price("STORAGE_1", BigDecimal(50), TradableType.UPGRADE))
                add(Price("ENERGY_REGEN_2", BigDecimal(300), TradableType.UPGRADE))
                add(Price("ENERGY_REGEN_1", BigDecimal(50), TradableType.UPGRADE))
                add(Price("STORAGE_3", BigDecimal(1500), TradableType.UPGRADE))
                add(Price("ENERGY_REGEN_4", BigDecimal(4000), TradableType.UPGRADE))
                add(Price("STORAGE_2", BigDecimal(300), TradableType.UPGRADE))
                add(Price("ENERGY_REGEN_3", BigDecimal(1500), TradableType.UPGRADE))
                add(Price("STORAGE_5", BigDecimal(15000), TradableType.UPGRADE))
                add(Price("STORAGE_4", BigDecimal(4000), TradableType.UPGRADE))
                add(Price("MINING_3", BigDecimal(1500), TradableType.UPGRADE))
                add(Price("MINING_4", BigDecimal(4000), TradableType.UPGRADE))
                add(Price("ROBOT", BigDecimal(100), TradableType.ITEM))
                add(Price("MINING_5", BigDecimal(15000), TradableType.UPGRADE))
                add(Price("MINING_1", BigDecimal(50), TradableType.UPGRADE))
                add(Price("MINING_2", BigDecimal(300), TradableType.UPGRADE))
                add(Price("IRON", BigDecimal(15), TradableType.RESOURCE))
                add(Price("HEALTH_2", BigDecimal(300), TradableType.UPGRADE))
                add(Price("HEALTH_1", BigDecimal(50), TradableType.UPGRADE))
                add(Price("GEM", BigDecimal(30), TradableType.RESOURCE))
                add(Price("HEALTH_5", BigDecimal(15000), TradableType.UPGRADE))
                add(Price("ENERGY_RESTORE", BigDecimal(75), TradableType.RESTORATION))
                add(Price("HEALTH_4", BigDecimal(4000), TradableType.UPGRADE))
                add(Price("HEALTH_3", BigDecimal(1500), TradableType.UPGRADE))
                add(Price("DAMAGE_5", BigDecimal(15000), TradableType.UPGRADE))
                add(Price("DAMAGE_4", BigDecimal(4000), TradableType.UPGRADE))
                add(Price("DAMAGE_3", BigDecimal(1500), TradableType.UPGRADE))
                add(Price("DAMAGE_2", BigDecimal(300), TradableType.UPGRADE))
                add(Price("DAMAGE_1", BigDecimal(50), TradableType.UPGRADE))
            }
        }
    }
    
}